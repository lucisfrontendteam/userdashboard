import React, { PureComponent } from 'react';
import { version as dashboardVersion } from '../../package.json';
import UIContents from '../Utils/UIContentsLoader';
const { version: uicontentsVersion } = UIContents;

export default class VersionViewer extends PureComponent {
  render() {
    const uicontentsInfo = `UIContents.js: ver ${uicontentsVersion}`;
    const dashboardInfo = `DashBoard.js: ver ${dashboardVersion}`;
    return (
      <ul style={{ float: 'left' }}>
        <li className="dropdown filter-menu">
          <span>{uicontentsInfo}</span>
        </li>
        <li className="dropdown filter-menu">
          <span>{dashboardInfo}</span>
        </li>
      </ul>
    );
  }
}
