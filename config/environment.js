var environment = {
  development: {
    rootPath: '/player',
  },
  production: {
    rootPath: '/player',
  },
  dist: {
    rootPath: '/player',
  },
};

module.exports = environment;
