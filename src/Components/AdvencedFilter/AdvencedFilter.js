import React, { PureComponent } from 'react';
import Navigation from './Navigation';
import { v4 } from 'uuid';
import $ from 'jquery';
import 'jquery.hotkeys';
import _ from 'lodash';
import moment from 'moment';
import { Pane0 } from './Pane0.js';
import Pane1 from './Pane1';
import Pane2 from './Pane2';
// import Pane3 from './Pane3';
import Pane4 from './Pane4';
import './AdvencedFilterCustom.css';
import PathManager from '../../Utils/PathManager';

export class AdvencedFilter extends PureComponent {
  static defaultProps = {
    isTestMode: false,
    organizationListId: v4(),
    counselorListId: v4(),
    dateRangeRadioName: v4(),
    allDataRangeId: v4(),
    recenctDataRangeId: v4(),
    rangeDataRangeId: v4(),
    dateRangeSpinnerId: v4(),
    dateTimeSelectId: v4(),
    timeSlotSelectId: v4(),
    dupsSelectId: v4(),
    clientSpeechSelectId: v4(),
    csSpeechSelectId: v4(),
    hoSelectId: v4(),
    silenceSelectId: v4(),
    teltimeSelectId: v4(),
    customDataPaneId: v4(),
    targetCategoryListId: v4(),
    targetCategoryGroupListId: v4(),
  };

  handleReset() {
    const { props } = this;
    props.querystatemanagerclearInitAction(props);
  }

  get isLoggedIn() {
    const { props } = this;
    const { LoginManagerState } = props;
    const loggedIn = LoginManagerState.status === true;
    return loggedIn;
  }

  get isRouted() {
    const { props } = this;
    const pathManager = new PathManager(props);
    const { scenario } = pathManager.firstScenarioPathInfo;
    const scenarioUriFound = !!!_.isEmpty(scenario);
    return scenarioUriFound;
  }

  getTargetTree(id) {
    return $(document.getElementById(id));
  }

  getTargetJqTreeState(id) {
    const $targetTree = this.getTargetTree(id);
    const state = JSON.parse($targetTree.tree('toJson'));
    return state;
  }

  get targetOrganizationListState() {
    const { props } = this;
    const counselorList = this.getTargetJqTreeState(props.organizationListId);
    return counselorList;
  }

  get targetCounselorListState() {
    const { props } = this;
    const counselorList = this.getTargetJqTreeState(props.counselorListId);
    return counselorList;
  }

  getSelectedRadioState(name) {
    const targetState = $(`input[name="${name}"]:checked`).val();
    return targetState;
  }

  get targetDateRangeState() {
    const { props } = this;
    const rangeType = this.getSelectedRadioState(props.dateRangeRadioName);
    return rangeType;
  }

  getInputBoxState(id) {
    const { value } = document.getElementById(id);
    return value;
  }

  get recentDaysFilterState() {
    const { props } = this;
    const state = this.getInputBoxState(props.dateRangeSpinnerId);
    return _.toNumber(state);
  }

  getTimePickerState(selector) {
    const dateTime = $(selector).timepicker('getTime');
    const value = moment(dateTime).format('HHmm');
    return value;
  }

  get timeRangeFromFilterState() {
    const { props } = this;
    const selector = `#${props.dateTimeSelectId} .time.start`;
    const state = this.getTimePickerState(selector);
    return state;
  }

  get timeRangeToFilterState() {
    const { props } = this;
    const selector = `#${props.dateTimeSelectId} .time.end`;
    const state = this.getTimePickerState(selector);
    return state;
  }

  getDatePickerState(selector) {
    const date = $(selector).datepicker('getDate');
    const value = moment(date).format('YYYYMMDD');
    return value;
  }

  get dateRangeFromFilterState() {
    const { props } = this;
    const selector = `#${props.dateTimeSelectId} .date.start`;
    const state = this.getDatePickerState(selector);
    return state;
  }

  get dateRangeToFilterState() {
    const { props } = this;
    const selector = `#${props.dateTimeSelectId} .date.end`;
    const state = this.getDatePickerState(selector);
    return state;
  }

  getSelectedOptionState(id) {
    const selector = `#${id} option:selected`;
    const timeslot = $(selector).data('timeslot');
    return timeslot;
  }

  get targetTimeSlotState() {
    const { props } = this;
    const state = this.getSelectedOptionState(props.timeSlotSelectId);
    return state;
  }

  get targetDupsFilterState() {
    const { props } = this;
    const state = this.getSelectedOptionState(props.dupsSelectId);
    return state;
  }

  get targetClientSpeechFilterState() {
    const { props } = this;
    const state = this.getSelectedOptionState(props.clientSpeechSelectId);
    return state;
  }

  get targetCsSpeechFilterState() {
    const { props } = this;
    const state = this.getSelectedOptionState(props.csSpeechSelectId);
    return state;
  }

  get targetHoFilterState() {
    const { props } = this;
    const state = this.getSelectedOptionState(props.hoSelectId);
    return state;
  }

  get targetSilenceFilterState() {
    const { props } = this;
    const state = this.getSelectedOptionState(props.silenceSelectId);
    return state;
  }

  get targetTelTimeFilterState() {
    const { props } = this;
    const state = this.getSelectedOptionState(props.teltimeSelectId);
    return state;
  }

  get customDataTreeChildrenState() {
    const { props } = this;
    const state = [];
    $(document.getElementById(props.customDataPaneId))
      .find('option[value!="0"]:selected')
      .each((index, select) => {
        const { model, parentid } = $(select).data();
        const { id, name } = model;
        const optionState = {
          id,
          name,
          parentid,
        };
        state.push(optionState);
      });
    return state;
  }

  get categoryGroupFilterListState() {
    const { props } = this;
    const categoryGroupList
      = this.getTargetJqTreeState(props.targetCategoryGroupListId);
    return categoryGroupList;
  }

  get categoryFilterListState() {
    const { props } = this;
    const categoryList
      = this.getTargetJqTreeState(props.targetCategoryListId);
    return categoryList;
  }

  get newQueryState() {
    const {
      targetOrganizationListState,
      targetCounselorListState,
      targetDateRangeState,
      recentDaysFilterState,
      timeRangeFromFilterState,
      timeRangeToFilterState,
      dateRangeFromFilterState,
      dateRangeToFilterState,
      targetTimeSlotState,
      targetDupsFilterState,
      targetClientSpeechFilterState,
      targetCsSpeechFilterState,
      targetHoFilterState,
      targetSilenceFilterState,
      targetTelTimeFilterState,
      customDataTreeChildrenState,
      categoryGroupFilterListState,
      categoryFilterListState,
    } = this;
    const newQueryState = {
      targetOrganizationListState,
      targetCounselorListState,
      targetDateRangeState,
      recentDaysFilterState,
      timeRangeFromFilterState,
      timeRangeToFilterState,
      dateRangeFromFilterState,
      dateRangeToFilterState,
      targetTimeSlotState,
      targetDupsFilterState,
      targetClientSpeechFilterState,
      targetCsSpeechFilterState,
      targetHoFilterState,
      targetSilenceFilterState,
      targetTelTimeFilterState,
      customDataTreeChildrenState,
      categoryGroupFilterListState,
      categoryFilterListState,
    };
    return newQueryState;
  }

  saveQueryState() {
    const { props, newQueryState } = this;
    if (props.isTestMode) {
      props.querystatemanagerupdateInitAction(props, newQueryState);
    } else {
      if (this.isLoggedIn && this.isRouted) {
        props.querystatemanagerupdateInitAction(props, newQueryState);
      }
    }
  }

  handleApply() {
    this.saveQueryState();
    const { props } = this;
    if (!!!props.isTestMode) {
      $('#searchFilterLayer').modal('hide');
    }
  }

  handleFilterShortCut() {
    $('#searchFilterLayer').modal('show');
  }

  componentDidMount() {
    $(document).bind('keydown', 'ctrl+f', this.handleFilterShortCut);
  }

  componentWillUnmount() {
    $(document).unbind('keydown', 'ctrl+f', this.handleFilterShortCut);
  }

  render() {
    const { props } = this;
    const modalClassNames = props.isTestMode ? '' : 'modal fade';
    return (
      <div className={modalClassNames} tabIndex="-1" role="dialog" id="searchFilterLayer">
        <div className="modal-dialog" role="document">
          <section className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <h1 className="modal-title">검색필터 설정</h1>
            </div>
            <div className="modal-body">
              <div className="filter-wrap">
                <Navigation {...props} />
                <div className="filter-content">
                  <div className="tab-content">
                    <Pane0 {...props} />
                    <Pane1 {...props} />
                    <Pane2 {...props} />
                    <Pane4 {...props} />
                  </div>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <p className="filter-reset">
                <a onClick={() => this.handleReset()}>필터 전체 초기화</a>
              </p>
              <button
                type="button"
                className="btn btn-default btn-modal"
                onClick={() => this.handleApply()}
              >적용</button>
              <button
                type="button"
                className="btn btn-default btn-modal"
                data-dismiss="modal"
              >닫기</button>
            </div>
          </section>
        </div>
      </div>
    );
  }
}
