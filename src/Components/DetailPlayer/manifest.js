export default {
  id: 'DetailPlayer',
  stateId: 'DetailPlayer',
  name: '청취',
  usage: '읽기',
  recordCount: 25,
  type: 'IdList',
  inputIdList: [
    {
      key: 'userId',
      id: 'detailPlayerUserId',
    },
    {
      key: 'projectId',
      id: 'detailPlayerProjectId',
    },
    {
      key: 'scenarioId',
      id: 'detailPlayerScenarioId',
    },
    {
      key: 'tabId',
      id: 'detailPlayerTabId',
    },
    {
      key: 'CategoryFilterListStateId',
      id: 'detailPlayerCategory1FilterListState',
    },
    {
      key: 'TargetHoFilterStateId',
      id: 'detailPlayerTargetHoFilterState',
    },
    {
      key: 'dateFrom',
      id: 'detailPlayerdateFrom',
    },
    {
      key: 'dateTo',
      id: 'detailPlayerdateTo',
    },
    {
      key: 'TargetSilenceFilterStateId',
      id: 'detailPlayerTargetSilenceFilterState',
    },
    {
      key: 'TargetTelTimeFilterStateId',
      id: 'detailPlayerTargetTelTimeFilterState',
    },
    {
      key: 'pageNumber',
      id: 'detailPlayerpageNumber',
    },
    {
      key: 'pageSize',
      id: 'detailPlayerpageSize',
    },
  ],
  method: 'post',
  apiUrl: '/speech/detailPlayer.api',
};
