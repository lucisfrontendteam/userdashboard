import * as actionTypes from '../actionTypes';

export default function WidgetViewerLayoutListState(states = [], action = null) {
  let finalState = [];
  switch (action.type) {
    case actionTypes.WIDGET_VIEWER_THEME_LIST_INIT: {
      const { widgetViewerThemeListState } = action.payload;
      finalState = finalState.concat(widgetViewerThemeListState);
    } break;

    default: {
      finalState = finalState.concat(states);
    }
  }

  return finalState;
}
