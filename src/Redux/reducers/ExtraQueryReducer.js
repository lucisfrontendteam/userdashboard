import * as actionTypes from '../actionTypes';

export default function ExtraQueryState(state = {}, action = null) {
  let finalState = {};
  switch (action.type) {
    case actionTypes.EXTRA_QUERY_INIT: {
      finalState = {};
    } break;

    case actionTypes.EXTRA_CATEGORYFILTERLISTSTATE_INIT: {
      const { CategoryFilterListStateId: id } = action.payload;
      finalState = {
        ...state,
        CategoryFilterListState: { id },
      };
    } break;

    default: {
      finalState = { ...state };
    }
  }
  return finalState;
}
