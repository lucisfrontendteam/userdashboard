import _ from 'lodash';
import UIContentsLoader from './UIContentsLoader';
const { EnvironmentManager } = UIContentsLoader;
import environment from '../../config/environment';

export default class PathManager {
  constructor(props) {
    this.props = props;
  }

  get rootPath() {
    const { rootPath } = new EnvironmentManager(environment);
    return rootPath;
  }

  get loginPath() {
    const { loginPath } = new EnvironmentManager(environment);
    return loginPath;
  }

  get errorPath() {
    const { errorPath } = new EnvironmentManager(environment);
    return errorPath;
  }

  get projectRootPath() {
    const { props } = this;
    const { ProjectState } = props;
    let projectPath = this.errorPath;
    if (!!!_.isEmpty(ProjectState)) {
      const { accessPath } = ProjectState;
      projectPath = `${this.rootPath}/${accessPath}`;
    }
    return projectPath;
  }

  get projectSelectPath() {
    return `${this.rootPath}/projectSelect`;
  }

  get firstScenarioRootPath() {
    const { props } = this;
    const { ProjectState } = props;
    let firstScenarioRootPath = this.errorPath;
    if (!!!_.isEmpty(ProjectState)) {
      const firstScenarioRoot = ProjectState.children[0];
      firstScenarioRootPath = `${this.projectRootPath}/${firstScenarioRoot.uri}`;
    }
    return firstScenarioRootPath;
  }

  get firstScenarioPathInfo() {
    const { props } = this;
    const { ProjectState } = props;
    let firstScenarioPath = this.errorPath;
    let firstScenario = '';
    if (this.firstScenarioRootPath !== this.errorPath) {
      if (!!!_.isEmpty(ProjectState)) {
        const [firstScenarioRoot] = ProjectState.children;
        firstScenario = firstScenarioRoot.children[0];
        if (!!!_.isEmpty(firstScenario)) {
          firstScenarioPath = `${this.firstScenarioRootPath}/${firstScenario.uri}`;
        }
      }
    }
    return {
      path: firstScenarioPath,
      scenario: firstScenario,
    };
  }
}
