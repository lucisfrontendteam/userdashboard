import * as actionTypes from './actionTypes';

export function AdvencedFilterQuickViewerState(state = {}, action = null) {
  let finalState = {};
  switch (action.type) {
    case actionTypes.AFQ_INIT: {
      const { widgetState } = action.payload;
      finalState = { ...widgetState };
    } break;

    case actionTypes.AFQ_CLEAR: {
      finalState = {};
    } break;

    default: {
      finalState = { ...state };
    }
  }

  return finalState;
}
