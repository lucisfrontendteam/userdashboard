import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { actions } from '../../Redux';
import _ from 'lodash';
import { v4 } from 'uuid';
import $ from 'jquery';

class Pane2SelectBox extends PureComponent {
  renderSelectOptions(itemList) {
    return _.map(itemList, item => {
      const stringId = _.toString(item.id);
      const data = JSON.stringify(item);
      return (
        <option
          key={v4()}
          data-timeslot={data}
          value={stringId}
        >{item.name}</option>
      );
    });
  }

  componentDidUpdate() {
    const { props } = this;
    const { selectedItem, selectBoxId } = props;
    if (selectedItem && selectBoxId) {
      const { id } = selectedItem;
      let selectedItemId = id;
      if (_.isEmpty(selectedItemId)) {
        selectedItemId = '0';
      }
      $(`#${selectBoxId}`).val(selectedItemId);
    }
  }

  render() {
    const { props } = this;
    const { itemList, selectBoxId } = props;
    return (
      <select
        className="form-control"
        id={selectBoxId}
      >
        {this.renderSelectOptions(itemList)}
      </select>
    );
  }
}

const mapStateToProps = (state, props) => {
  const { filterTypes, type } = props;
  const {
    hoDirectionList,
    dupsRatioList,
    clientSpeechRatioList,
    csSpeechRatioList,
    silenceRatioList,
    intervalList,
  } = props.AdvencedFilterArgManagerState;
  const dummy = { id: '' };
  const {
    targetDupsFilterState = dummy,
    targetClientSpeechFilterState = dummy,
    targetCsSpeechFilterState = dummy,
    targetHoFilterState = dummy,
    targetSilenceFilterState = dummy,
    targetTelTimeFilterState = dummy,
  } = props.QueryStateManagerState;
  let itemList = '';
  let selectedItem = '';
  let selectBoxId = '';
  // eslint-disable-next-line
  switch (type) {
    case filterTypes.DUPS:
      itemList = dupsRatioList;
      selectedItem = targetDupsFilterState;
      selectBoxId = props.dupsSelectId;
      break;
    case filterTypes.CLIENTSPEECH:
      itemList = clientSpeechRatioList;
      selectedItem = targetClientSpeechFilterState;
      selectBoxId = props.clientSpeechSelectId;
      break;
    case filterTypes.SCSPEECH:
      itemList = csSpeechRatioList;
      selectedItem = targetCsSpeechFilterState;
      selectBoxId = props.csSpeechSelectId;
      break;
    case filterTypes.HO:
      itemList = hoDirectionList;
      selectedItem = targetHoFilterState;
      selectBoxId = props.hoSelectId;
      break;
    case filterTypes.SILENCE:
      itemList = silenceRatioList;
      selectedItem = targetSilenceFilterState;
      selectBoxId = props.silenceSelectId;
      break;
    case filterTypes.TELTIME:
      itemList = intervalList;
      selectedItem = targetTelTimeFilterState;
      selectBoxId = props.teltimeSelectId;
      break;
  }

  return {
    itemList,
    selectedItem,
    selectBoxId,
    ...props,
  };
};
const connectedPane2SelectBox
  = connect(mapStateToProps, actions)(Pane2SelectBox);
export default connectedPane2SelectBox;
