/**
 * 화면 식별자: 청취화면
 */
export default {
  grid: {
    // 청취화면 그리드의 데이터 배열(일련번호 오름차순)
    totalCount: 100,
    datas: [
      {
        // 일련번호
        no: 1,
        // 통화일시 (YYYYMMDDhmmss 포맷)
        datetime: '20160926092811',
        // 상담사 이름
        counselorName: '김성원',
        // 통화시간 (단위는 초)
        telTimes: 5000,
        // 묶음비율
        packRatio: 0.12,
        // 묶음횟수 (단위는 건)
        packCount: 10,
        // 카테고리
        category: '외환',
        // 검출 키워드
        keyword: '환전',
        // 상담사 발화비율
        counselorRatio: 0.32,
        // 고객 발화비율
        customerRatio: 0.65,
        // 말겹침 비율
        dupsRatio: 0.2,
      }, {
        // 일련번호
        no: 2,
        // 통화일시 (YYYYMMDDhmmss 포맷)
        datetime: '20160926092811',
        // 상담사 이름
        counselorName: '김성원',
        // 통화시간 (단위는 초)
        telTimes: 5000,
        // 묶음비율
        packRatio: 0.12,
        // 묶음횟수 (단위는 건)
        packCount: 10,
        // 카테고리
        category: '외환',
        // 검출 키워드
        keyword: '환전',
        // 상담사 발화비율
        counselorRatio: 0.32,
        // 고객 발화비율
        customerRatio: 0.65,
        // 말겹침 비율
        dupsRatio: 0.2,
      }, {
        // 일련번호
        no: 3,
        // 통화일시 (YYYYMMDDhmmss 포맷)
        datetime: '20160926092811',
        // 상담사 이름
        counselorName: '김성원',
        // 통화시간 (단위는 초)
        telTimes: 5000,
        // 묶음비율
        packRatio: 0.12,
        // 묶음횟수 (단위는 건)
        packCount: 10,
        // 카테고리
        category: '외환',
        // 검출 키워드
        keyword: '환전',
        // 상담사 발화비율
        counselorRatio: 0.32,
        // 고객 발화비율
        customerRatio: 0.65,
        // 말겹침 비율
        dupsRatio: 0.2,
      }, {
        // 일련번호
        no: 4,
        // 통화일시 (YYYYMMDDhmmss 포맷)
        datetime: '20160926092811',
        // 상담사 이름
        counselorName: '김성원',
        // 통화시간 (단위는 초)
        telTimes: 5000,
        // 묶음비율
        packRatio: 0.12,
        // 묶음횟수 (단위는 건)
        packCount: 10,
        // 카테고리
        category: '외환',
        // 검출 키워드
        keyword: '환전',
        // 상담사 발화비율
        counselorRatio: 0.32,
        // 고객 발화비율
        customerRatio: 0.65,
        // 말겹침 비율
        dupsRatio: 0.2,
      }, {
        // 일련번호
        no: 5,
        // 통화일시 (YYYYMMDDhmmss 포맷)
        datetime: '20160926092811',
        // 상담사 이름
        counselorName: '김성원',
        // 통화시간 (단위는 초)
        telTimes: 5000,
        // 묶음비율
        packRatio: 0.12,
        // 묶음횟수 (단위는 건)
        packCount: 10,
        // 카테고리
        category: '외환',
        // 검출 키워드
        keyword: '환전',
        // 상담사 발화비율
        counselorRatio: 0.32,
        // 고객 발화비율
        customerRatio: 0.65,
        // 말겹침 비율
        dupsRatio: 0.2,
      }, {
        // 일련번호
        no: 6,
        // 통화일시 (YYYYMMDDhmmss 포맷)
        datetime: '20160926092811',
        // 상담사 이름
        counselorName: '김성원',
        // 통화시간 (단위는 초)
        telTimes: 5000,
        // 묶음비율
        packRatio: 0.12,
        // 묶음횟수 (단위는 건)
        packCount: 10,
        // 카테고리
        category: '외환',
        // 검출 키워드
        keyword: '환전',
        // 상담사 발화비율
        counselorRatio: 0.32,
        // 고객 발화비율
        customerRatio: 0.65,
        // 말겹침 비율
        dupsRatio: 0.2,
      }, {
        // 일련번호
        no: 7,
        // 통화일시 (YYYYMMDDhmmss 포맷)
        datetime: '20160926092811',
        // 상담사 이름
        counselorName: '김성원',
        // 통화시간 (단위는 초)
        telTimes: 5000,
        // 묶음비율
        packRatio: 0.12,
        // 묶음횟수 (단위는 건)
        packCount: 10,
        // 카테고리
        category: '외환',
        // 검출 키워드
        keyword: '환전',
        // 상담사 발화비율
        counselorRatio: 0.32,
        // 고객 발화비율
        customerRatio: 0.65,
        // 말겹침 비율
        dupsRatio: 0.2,
      }, {
        // 일련번호
        no: 8,
        // 통화일시 (YYYYMMDDhmmss 포맷)
        datetime: '20160926092811',
        // 상담사 이름
        counselorName: '김성원',
        // 통화시간 (단위는 초)
        telTimes: 5000,
        // 묶음비율
        packRatio: 0.12,
        // 묶음횟수 (단위는 건)
        packCount: 10,
        // 카테고리
        category: '외환',
        // 검출 키워드
        keyword: '환전',
        // 상담사 발화비율
        counselorRatio: 0.32,
        // 고객 발화비율
        customerRatio: 0.65,
        // 말겹침 비율
        dupsRatio: 0.2,
      }, {
        // 일련번호
        no: 9,
        // 통화일시 (YYYYMMDDhmmss 포맷)
        datetime: '20160926092811',
        // 상담사 이름
        counselorName: '김성원',
        // 통화시간 (단위는 초)
        telTimes: 5000,
        // 묶음비율
        packRatio: 0.12,
        // 묶음횟수 (단위는 건)
        packCount: 10,
        // 카테고리
        category: '외환',
        // 검출 키워드
        keyword: '환전',
        // 상담사 발화비율
        counselorRatio: 0.32,
        // 고객 발화비율
        customerRatio: 0.65,
        // 말겹침 비율
        dupsRatio: 0.2,
      }, {
        // 일련번호
        no: 10,
        // 통화일시 (YYYYMMDDhmmss 포맷)
        datetime: '20160926092811',
        // 상담사 이름
        counselorName: '김성원',
        // 통화시간 (단위는 초)
        telTimes: 5000,
        // 묶음비율
        packRatio: 0.12,
        // 묶음횟수 (단위는 건)
        packCount: 10,
        // 카테고리
        category: '외환',
        // 검출 키워드
        keyword: '환전',
        // 상담사 발화비율
        counselorRatio: 0.32,
        // 고객 발화비율
        customerRatio: 0.65,
        // 말겹침 비율
        dupsRatio: 0.2,
      }, {
        // 일련번호
        no: 11,
        // 통화일시 (YYYYMMDDhmmss 포맷)
        datetime: '20160926092811',
        // 상담사 이름
        counselorName: '김성원',
        // 통화시간 (단위는 초)
        telTimes: 5000,
        // 묶음비율
        packRatio: 0.12,
        // 묶음횟수 (단위는 건)
        packCount: 10,
        // 카테고리
        category: '외환',
        // 검출 키워드
        keyword: '환전',
        // 상담사 발화비율
        counselorRatio: 0.32,
        // 고객 발화비율
        customerRatio: 0.65,
        // 말겹침 비율
        dupsRatio: 0.2,
      }, {
        // 일련번호
        no: 12,
        // 통화일시 (YYYYMMDDhmmss 포맷)
        datetime: '20160926092811',
        // 상담사 이름
        counselorName: '김성원',
        // 통화시간 (단위는 초)
        telTimes: 5000,
        // 묶음비율
        packRatio: 0.12,
        // 묶음횟수 (단위는 건)
        packCount: 10,
        // 카테고리
        category: '외환',
        // 검출 키워드
        keyword: '환전',
        // 상담사 발화비율
        counselorRatio: 0.32,
        // 고객 발화비율
        customerRatio: 0.65,
        // 말겹침 비율
        dupsRatio: 0.2,
      }, {
        // 일련번호
        no: 13,
        // 통화일시 (YYYYMMDDhmmss 포맷)
        datetime: '20160926092811',
        // 상담사 이름
        counselorName: '김성원',
        // 통화시간 (단위는 초)
        telTimes: 5000,
        // 묶음비율
        packRatio: 0.12,
        // 묶음횟수 (단위는 건)
        packCount: 10,
        // 카테고리
        category: '외환',
        // 검출 키워드
        keyword: '환전',
        // 상담사 발화비율
        counselorRatio: 0.32,
        // 고객 발화비율
        customerRatio: 0.65,
        // 말겹침 비율
        dupsRatio: 0.2,
      }, {
        // 일련번호
        no: 14,
        // 통화일시 (YYYYMMDDhmmss 포맷)
        datetime: '20160926092811',
        // 상담사 이름
        counselorName: '김성원',
        // 통화시간 (단위는 초)
        telTimes: 5000,
        // 묶음비율
        packRatio: 0.12,
        // 묶음횟수 (단위는 건)
        packCount: 10,
        // 카테고리
        category: '외환',
        // 검출 키워드
        keyword: '환전',
        // 상담사 발화비율
        counselorRatio: 0.32,
        // 고객 발화비율
        customerRatio: 0.65,
        // 말겹침 비율
        dupsRatio: 0.2,
      }, {
        // 일련번호
        no: 15,
        // 통화일시 (YYYYMMDDhmmss 포맷)
        datetime: '20160926092811',
        // 상담사 이름
        counselorName: '김성원',
        // 통화시간 (단위는 초)
        telTimes: 5000,
        // 묶음비율
        packRatio: 0.12,
        // 묶음횟수 (단위는 건)
        packCount: 10,
        // 카테고리
        category: '외환',
        // 검출 키워드
        keyword: '환전',
        // 상담사 발화비율
        counselorRatio: 0.32,
        // 고객 발화비율
        customerRatio: 0.65,
        // 말겹침 비율
        dupsRatio: 0.2,
      }, {
        // 일련번호
        no: 16,
        // 통화일시 (YYYYMMDDhmmss 포맷)
        datetime: '20160926092811',
        // 상담사 이름
        counselorName: '김성원',
        // 통화시간 (단위는 초)
        telTimes: 5000,
        // 묶음비율
        packRatio: 0.12,
        // 묶음횟수 (단위는 건)
        packCount: 10,
        // 카테고리
        category: '외환',
        // 검출 키워드
        keyword: '환전',
        // 상담사 발화비율
        counselorRatio: 0.32,
        // 고객 발화비율
        customerRatio: 0.65,
        // 말겹침 비율
        dupsRatio: 0.2,
      }, {
        // 일련번호
        no: 17,
        // 통화일시 (YYYYMMDDhmmss 포맷)
        datetime: '20160926092811',
        // 상담사 이름
        counselorName: '김성원',
        // 통화시간 (단위는 초)
        telTimes: 5000,
        // 묶음비율
        packRatio: 0.12,
        // 묶음횟수 (단위는 건)
        packCount: 10,
        // 카테고리
        category: '외환',
        // 검출 키워드
        keyword: '환전',
        // 상담사 발화비율
        counselorRatio: 0.32,
        // 고객 발화비율
        customerRatio: 0.65,
        // 말겹침 비율
        dupsRatio: 0.2,
      }, {
        // 일련번호
        no: 18,
        // 통화일시 (YYYYMMDDhmmss 포맷)
        datetime: '20160926092811',
        // 상담사 이름
        counselorName: '김성원',
        // 통화시간 (단위는 초)
        telTimes: 5000,
        // 묶음비율
        packRatio: 0.12,
        // 묶음횟수 (단위는 건)
        packCount: 10,
        // 카테고리
        category: '외환',
        // 검출 키워드
        keyword: '환전',
        // 상담사 발화비율
        counselorRatio: 0.32,
        // 고객 발화비율
        customerRatio: 0.65,
        // 말겹침 비율
        dupsRatio: 0.2,
      }, {
        // 일련번호
        no: 19,
        // 통화일시 (YYYYMMDDhmmss 포맷)
        datetime: '20160926092811',
        // 상담사 이름
        counselorName: '김성원',
        // 통화시간 (단위는 초)
        telTimes: 5000,
        // 묶음비율
        packRatio: 0.12,
        // 묶음횟수 (단위는 건)
        packCount: 10,
        // 카테고리
        category: '외환',
        // 검출 키워드
        keyword: '환전',
        // 상담사 발화비율
        counselorRatio: 0.32,
        // 고객 발화비율
        customerRatio: 0.65,
        // 말겹침 비율
        dupsRatio: 0.2,
      }, {
        // 일련번호
        no: 20,
        // 통화일시 (YYYYMMDDhmmss 포맷)
        datetime: '20160926092811',
        // 상담사 이름
        counselorName: '김성원',
        // 통화시간 (단위는 초)
        telTimes: 5000,
        // 묶음비율
        packRatio: 0.12,
        // 묶음횟수 (단위는 건)
        packCount: 10,
        // 카테고리
        category: '외환',
        // 검출 키워드
        keyword: '환전',
        // 상담사 발화비율
        counselorRatio: 0.32,
        // 고객 발화비율
        customerRatio: 0.65,
        // 말겹침 비율
        dupsRatio: 0.2,
      }, {
        // 일련번호
        no: 21,
        // 통화일시 (YYYYMMDDhmmss 포맷)
        datetime: '20160926092811',
        // 상담사 이름
        counselorName: '김성원',
        // 통화시간 (단위는 초)
        telTimes: 5000,
        // 묶음비율
        packRatio: 0.12,
        // 묶음횟수 (단위는 건)
        packCount: 10,
        // 카테고리
        category: '외환',
        // 검출 키워드
        keyword: '환전',
        // 상담사 발화비율
        counselorRatio: 0.32,
        // 고객 발화비율
        customerRatio: 0.65,
        // 말겹침 비율
        dupsRatio: 0.2,
      }, {
        // 일련번호
        no: 22,
        // 통화일시 (YYYYMMDDhmmss 포맷)
        datetime: '20160926092811',
        // 상담사 이름
        counselorName: '김성원',
        // 통화시간 (단위는 초)
        telTimes: 5000,
        // 묶음비율
        packRatio: 0.12,
        // 묶음횟수 (단위는 건)
        packCount: 10,
        // 카테고리
        category: '외환',
        // 검출 키워드
        keyword: '환전',
        // 상담사 발화비율
        counselorRatio: 0.32,
        // 고객 발화비율
        customerRatio: 0.65,
        // 말겹침 비율
        dupsRatio: 0.2,
      }, {
        // 일련번호
        no: 23,
        // 통화일시 (YYYYMMDDhmmss 포맷)
        datetime: '20160926092811',
        // 상담사 이름
        counselorName: '김성원',
        // 통화시간 (단위는 초)
        telTimes: 5000,
        // 묶음비율
        packRatio: 0.12,
        // 묶음횟수 (단위는 건)
        packCount: 10,
        // 카테고리
        category: '외환',
        // 검출 키워드
        keyword: '환전',
        // 상담사 발화비율
        counselorRatio: 0.32,
        // 고객 발화비율
        customerRatio: 0.65,
        // 말겹침 비율
        dupsRatio: 0.2,
      }, {
        // 일련번호
        no: 24,
        // 통화일시 (YYYYMMDDhmmss 포맷)
        datetime: '20160926092811',
        // 상담사 이름
        counselorName: '김성원',
        // 통화시간 (단위는 초)
        telTimes: 5000,
        // 묶음비율
        packRatio: 0.12,
        // 묶음횟수 (단위는 건)
        packCount: 10,
        // 카테고리
        category: '외환',
        // 검출 키워드
        keyword: '환전',
        // 상담사 발화비율
        counselorRatio: 0.32,
        // 고객 발화비율
        customerRatio: 0.65,
        // 말겹침 비율
        dupsRatio: 0.2,
      }, {
        // 일련번호
        no: 25,
        // 통화일시 (YYYYMMDDhmmss 포맷)
        datetime: '20160926092811',
        // 상담사 이름
        counselorName: '김성원',
        // 통화시간 (단위는 초)
        telTimes: 5000,
        // 묶음비율
        packRatio: 0.12,
        // 묶음횟수 (단위는 건)
        packCount: 10,
        // 카테고리
        category: '외환',
        // 검출 키워드
        keyword: '환전',
        // 상담사 발화비율
        counselorRatio: 0.32,
        // 고객 발화비율
        customerRatio: 0.65,
        // 말겹침 비율
        dupsRatio: 0.2,
      },
    ],
  },
};
