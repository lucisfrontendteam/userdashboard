import UIContentsLoader from '../../Utils/UIContentsLoader';
const { TextRenderer } = UIContentsLoader;

export default {
  grid: {
    options: {
      paging: false,
      columnDefs: [
        {
          title: 'No',
          targets: 0,
          width: '5%',
          render: TextRenderer.renderText,
        },
        {
          title: '통화일시',
          targets: 1,
          width: 'auto',
          render: TextRenderer.renderText,
        },
        {
          title: '상담사',
          targets: 2,
          width: '7%',
          render: TextRenderer.renderText,
        },
        {
          title: '통화시간',
          targets: 3,
          width: '7%',
          render: TextRenderer.renderLocaleDateDuration,
        },
        {
          title: '묶음비율',
          targets: 4,
          width: '7%',
          render: TextRenderer.renderPercentage,
        },
        {
          title: '카테고리',
          targets: 5,
          width: '7%',
          render: TextRenderer.renderText,
        },
        {
          title: '검출 키워드',
          targets: 6,
          width: '8%',
          render: TextRenderer.renderText,
        },
        {
          title: '고객 발화비율',
          targets: 7,
          width: '9%',
          render: TextRenderer.renderPercentage,
        },
        {
          title: '상담사 발화비율',
          targets: 8,
          width: '10%',
          render: TextRenderer.renderPercentage,
        },
        {
          title: '말겹침 비율',
          targets: 9,
          width: '8%',
          render: TextRenderer.renderPercentage,
        },
      ],
    },
  },
};
