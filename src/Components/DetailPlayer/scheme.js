/* eslint-disable */
export default {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "grid": {
      "type": "object",
      "properties": {
        "totalCount": {
          "type": "integer"
        },
        "datas": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "no": {
                "type": "integer"
              },
              "datetime": {
                "type": "string"
              },
              "counselorName": {
                "type": "string"
              },
              "telTimes": {
                "type": "integer"
              },
              "packRatio": {
                "type": "number"
              },
              "packCount": {
                "type": "integer"
              },
              "category": {
                "type": "string"
              },
              "keyword": {
                "type": "string"
              },
              "counselorRatio": {
                "type": "number"
              },
              "customerRatio": {
                "type": "number"
              },
              "dupsRatio": {
                "type": "number"
              }
            },
            "required": [
              "no",
              "datetime",
              "counselorName",
              "telTimes",
              "packRatio",
              "packCount",
              "category",
              "keyword",
              "counselorRatio",
              "customerRatio",
              "dupsRatio"
            ]
          }
        }
      },
      "required": [
        "totalCount",
        "datas"
      ]
    }
  },
  "required": [
    "grid"
  ]
};
