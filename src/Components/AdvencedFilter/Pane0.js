import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { actions } from '../../Redux';
import _ from 'lodash';
import $ from 'jquery';
import 'jqtree';
import AdvencedFilterUtil from './AdvencedFilterUtil';
import UIContents from '../../Utils/UIContentsLoader';
const { TreeTraveser } = UIContents;

class Pane0 extends PureComponent {
  get counselorNodeList() {
    const { props } = this;
    const { organizationTree } = props.AdvencedFilterArgManagerState;
    const {
      targetCounselorListState = [],
    } = props.QueryStateManagerState;
    const nodeList
      = AdvencedFilterUtil.getNodeListFromOriginalTree(
        targetCounselorListState, organizationTree
      );
    return nodeList;
  }

  get organizationNodeList() {
    const { props } = this;
    const { organizationTree } = props.AdvencedFilterArgManagerState;
    const {
      targetOrganizationListState = [],
    } = props.QueryStateManagerState;
    const nodeList
      = AdvencedFilterUtil.getNodeListFromOriginalTree(
        targetOrganizationListState, organizationTree
      );
    return nodeList;
  }

  initializeTrees() {
    const { props, counselorNodeList, organizationNodeList } = this;
    const { leavedOrganizationTree } = props.AdvencedFilterArgManagerState;

    this.$totalTree = $('#tree1-1').tree({
      data: leavedOrganizationTree,
      autoOpen: true,
      buttonLeft: false,
    });

    this.$counselorTree
      = $(document.getElementById(props.counselorListId)).tree({
        data: counselorNodeList,
        autoOpen: true,
        buttonLeft: false,
      });

    this.$orgTree
      = $(document.getElementById(props.organizationListId)).tree({
        data: organizationNodeList,
        autoOpen: true,
        buttonLeft: false,
      });
  }

  destroyTrees() {
    this.$totalTree.tree('destroy');
    this.$counselorTree.tree('destroy');
    this.$orgTree.tree('destroy');
  }

  componentDidMount() {
    this.initializeTrees();
  }

  componentDidUpdate() {
    this.destroyTrees();
    this.initializeTrees();
  }

  handleAddOnClick() {
    const { $totalTree } = this;
    const selectedNode = $totalTree.tree('getSelectedNode');
    if (selectedNode) {
      const isCounselor = selectedNode.children.length === 0;
      if (isCounselor) {
        this.addSelectedCounselorNode(selectedNode);
      } else {
        this.addSelectedOrgNode(selectedNode);
      }
    }
  }

  addSelectedNode(type, $targetTree, targetNode) {
    const listState = JSON.parse($targetTree.tree('toJson'));
    const dupNode = _.find(listState, node => node.id === targetNode.id);
    if (_.isUndefined(dupNode)) {
      const rootNode = $targetTree.tree('getTree');
      $targetTree.tree('appendNode', targetNode, rootNode);

      // const trimState = JSON.parse($targetTree.tree('toJson'));
      // const { props } = this;
      // if (type === 'org') {
      //   props.orgListInitAction(trimState);
      // } else {
      //   props.counselorListInitAction(trimState);
      // }
    }
  }

  addSelectedOrgNode(selectedOrgNode) {
    const { $orgTree } = this;
    const rootNode = {
      ...selectedOrgNode,
      children: undefined,
    };
    this.addSelectedNode('org', $orgTree, rootNode);
  }

  addSelectedCounselorNode(selectedCounselorNode) {
    const { $counselorTree } = this;
    this.addSelectedNode('counselor', $counselorTree, selectedCounselorNode);
  }

  handleRemoveOnClick() {
    // this.removeSelectedCounselorNode();
    // this.removeSelectedOrgNode();
    const { $counselorTree, $orgTree } = this;
    const selectedCounselorNode = $counselorTree.tree('getSelectedNode');
    this.removeSelectedCounselorNode(selectedCounselorNode);
    const selectedOrgNode = $orgTree.tree('getSelectedNode');
    this.removeSelectedOrgNode(selectedOrgNode);
  }

  // removeSelectedCounselorNode() {
  //   const { $counselorTree, props } = this;
  //   const listData = JSON.parse($counselorTree.tree('toJson'));
  //   // props.counselorListInitAction(listData);
  // }
  //
  // removeSelectedOrgNode() {
  //   const { $orgTree, props } = this;
  //   const listData = JSON.parse($orgTree.tree('toJson'));
  //   // props.orgListInitAction(listData);
  // }

  removeSelectedCounselorNode(selectedCounselorNode) {
    const { $counselorTree } = this;
    $counselorTree.tree('removeNode', selectedCounselorNode);
  }

  removeSelectedOrgNode(selectedOrgNode) {
    const { $orgTree } = this;
    $orgTree.tree('removeNode', selectedOrgNode);
  }

  handleTreeViewTypeToggle(viewType) {
    // const { props } = this;
    // props.organizationTreeViewTypeInitAction(viewType);
    this.setState({ viewType });
  }

  handleOrgSearch(e) {
    e.preventDefault();
    e.stopPropagation();
    const { props } = this;
    const { value } = document.getElementById('orgFilter');
    props.organizationTreeSearchKeywordInitAction(value);
  }

  get serachPlaceholder() {
    const { props } = this;
    const { OrganizationTreeViewTypeState } = props;

    let value = '상담사 이름으로 검색';
    if (OrganizationTreeViewTypeState === 'tree') {
      value = '조직 이름으로 검색';
    }
    return value;
  }

  render() {
    const { props } = this;
    return (
      <div role="tabpanel" className="tab-pane active" id="searchFilterSet01">
        <h2 className="title1">조직 / 상담</h2>
        <div className="row select-ui">
          <div className="col-md-6 list-left">
            <div className="section-wrap">
              <h3 className="title2">조직도</h3>
              <div className="innerbox filter-organization">
                <div className="header-area">
                  <div className="btn-group" data-toggle="buttons" style={{ display: 'none' }}>
                    <label
                      className="btn btn-img-organization active"
                      onClick={() => this.handleTreeViewTypeToggle('tree')}
                    >
                      <input
                        type="radio"
                        name="options"
                        id="option1"
                        autoComplete="off"
                      />
                    </label>
                    <label
                      className="btn btn-img-list"
                      onClick={() => this.handleTreeViewTypeToggle('flat')}
                    >
                      <input
                        type="radio"
                        name="options"
                        id="option2"
                        autoComplete="off"
                      />
                    </label>
                  </div>
                  <ul className="select-area">
                    <li className="search-box">
                      <form role="search" onSubmit={e => this.handleOrgSearch(e)}>
                        <input
                          type="text"
                          id="orgFilter"
                          className="form-control"
                          autoComplete="off"
                          placeholder={this.serachPlaceholder}
                        />
                        <button type="button">
                          <span
                            className="glyphicon glyphicon-search"
                            aria-hidden="true"
                          />
                        </button>
                      </form>
                    </li>
                  </ul>
                </div>
                <div className="scroll-y tree-list ico-user" id="tree1-1" />
              </div>
            </div>
          </div>
          <div className="col-md-6 list-right">
            <div className="row">
              <div className="col-md-12">
                <div className="section-wrap">
                  <h3 className="title2">대상 조직</h3>
                  <div
                    className="innerbox scroll-y tree-list ico-user small"
                    id={props.organizationListId}
                  />
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12">
                <div className="section-wrap">
                  <h3 className="title2">대상 상담사</h3>
                  <div
                    className="innerbox scroll-y tree-list ico-user small"
                    id={props.counselorListId}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="btn-area">
            <button
              type="button"
              onClick={() => this.handleAddOnClick()}
              className="btn-img-select right"
            >추가</button>
            <button
              type="button"
              onClick={() => this.handleRemoveOnClick()}
              className="btn-img-select left"
            >삭제</button>
          </div>
        </div>
      </div>
    );
  }
}

const setFlatViewOrganizationTreeState = state => {
  const {
    OrganizationTreeSearchKeywordState: keyword,
    AdvencedFilterArgManagerState,
  } = state;

  const { organizationTree } = AdvencedFilterArgManagerState;
  const treeTraveser = new TreeTraveser(organizationTree);
  let leavedOrganizationTree = treeTraveser.getChildNodesModel();
  if (!!!_.isEmpty(keyword)) {
    leavedOrganizationTree
      = _.filter(leavedOrganizationTree, model => _.includes(model.name, keyword));
  }

  return {
    ...state,
    AdvencedFilterArgManagerState: {
      ...AdvencedFilterArgManagerState,
      organizationTree,
      leavedOrganizationTree,
    },
  };
};

const setTreeViewOrganizationTreeState = state => {
  const {
    OrganizationTreeSearchKeywordState: keyword,
    AdvencedFilterArgManagerState,
  } = state;
  const { organizationTree = [] } = AdvencedFilterArgManagerState;
  let leavedOrganizationTree = [...organizationTree];

  if (!!!_.isEmpty(keyword)) {
    const treeTraveser = new TreeTraveser(organizationTree);
    leavedOrganizationTree = _.filter(
      treeTraveser.getRootNodesModel(),
      model => _.includes(model.name, keyword)
    );
  }

  return {
    ...state,
    AdvencedFilterArgManagerState: {
      ...AdvencedFilterArgManagerState,
      organizationTree,
      leavedOrganizationTree,
    },
  };
};

// const getTreeTypeState = state => {
//   const {
//     OrganizationTreeSearchKeywordState: keyword,
//     AdvencedFilterArgManagerState,
//   } = state;
//
//   const { organizationTree } = AdvencedFilterArgManagerState;
//   const treeTraveser = new TreeTraveser(organizationTree);
//   let leavedOrganizationTree = treeTraveser.getChildNodesModel();
//   if (!!!_.isEmpty(keyword)) {
//     leavedOrganizationTree
//       = _.filter(leavedOrganizationTree, model => _.includes(model.name, keyword));
//   }
//   return leavedOrganizationTree;
// };
//
// const getListTypeState = state => {
//   const {
//     OrganizationTreeSearchKeywordState: keyword,
//     AdvencedFilterArgManagerState,
//   } = state;
//
//   const { organizationTree } = AdvencedFilterArgManagerState;
//   const treeTraveser = new TreeTraveser(organizationTree);
//   let leavedOrganizationTree = treeTraveser.getChildNodesModel();
//   if (!!!_.isEmpty(keyword)) {
//     leavedOrganizationTree
//       = _.filter(leavedOrganizationTree, model => _.includes(model.name, keyword));
//   }
//   return leavedOrganizationTree;
// };
//
// const getNewState = state => {
//   const { AdvencedFilterArgManagerState } = state;
//   const { organizationTree } = AdvencedFilterArgManagerState;
//   const orgList = getListTypeState(state);
//   const orgTree = getTreeTypeState(state);
//   return {
//     ...state,
//     AdvencedFilterArgManagerState: {
//       ...AdvencedFilterArgManagerState,
//       organizationTree,
//       orgList,
//       orgTree,
//     },
//   };
// };

const mapStateToProps = (state) => {
  const { OrganizationTreeViewTypeState } = state;

  let newState = { ...state };
  if (OrganizationTreeViewTypeState === 'flat') {
    newState = setFlatViewOrganizationTreeState(newState);
  } else {
    newState = setTreeViewOrganizationTreeState(newState);
  }

  return newState;
};
const connectedPane0 = connect(mapStateToProps, actions)(Pane0);
export { connectedPane0 as Pane0 };
