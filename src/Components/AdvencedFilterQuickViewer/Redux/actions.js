import * as actionTypes from './actionTypes';
import DefaultStates from '../../../Redux/DefaultStates';
import UIContents from '../../../Utils/UIContentsLoader';
const { AjaxManager } = UIContents;
import { AdvencedFilterQuickViewerSampleDatas } from '../AdvencedFilterQuickViewerSampleDatas';
import manifest from '../manifest';

export function advencedfilterquickviewerInitAction(props) {
  return dispatch => {
    const { NetWorkModeState, QueryIdMap, QueryStateManagerState } = props;
    const { method, apiUrl } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.AFQ_INIT,
      query: {
        [DefaultStates.queryKey]: QueryStateManagerState,
        ...QueryIdMap,
      },
      ajaxType: 'normal',
      sampleData: AdvencedFilterQuickViewerSampleDatas,
    };
    AjaxManager.run(param);
  };
}
