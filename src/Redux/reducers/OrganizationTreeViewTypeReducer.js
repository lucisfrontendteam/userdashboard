import * as actionTypes from '../actionTypes';

export default function OrganizationTreeViewTypeReducer(state = '', action = null) {
  let finalState = '';
  switch (action.type) {
    case actionTypes.ORGANIZATION_TREE_VIEW_TYPE_INIT: {
      const { viewType } = action.payload;
      finalState = viewType;
    } break;

    default: {
      finalState = state;
    }
  }

  return finalState;
}
