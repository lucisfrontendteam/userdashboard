import * as actionTypes from '../actionTypes';
import UIContents from '../../Utils/UIContentsLoader';
const { ProjectWriter, CommonManagerActionTypeList } = UIContents;

/**
 * 시나리오 트리용 액션별 페이로드를 관리하는 리듀서를 생성하는 함수.
 *
 * @param state
 * @param action
 * @returns {Array}
 * @constructor
 */
export default function ProjectState(state = {}, action = null) {
  let finalState = {};
  switch (action.type) {
    case CommonManagerActionTypeList.LOGOUT: {
      finalState = {};
    } break;

    case actionTypes.PROJECT_INIT: {
      const { projectState } = action.payload;
      finalState = { ...projectState };
    } break;

    case actionTypes.SCENARIO_SELECT: {
      const { scenarioUri } = action.payload;
      const props = { ProjectState: state };
      const newProjectState = ProjectWriter.selectScenarioByUri(props, scenarioUri);
      finalState = { ...newProjectState };
    } break;

    default: {
      finalState = { ...state };
    }
  }

  return finalState;
}
