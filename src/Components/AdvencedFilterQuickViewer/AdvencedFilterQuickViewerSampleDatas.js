/**
 * 화면 식별자: 청취화면
 */
export const AdvencedFilterQuickViewerSampleDatas = {
  // 레코드의 총 갯수
  totalCount: 30000,
  // 필터된 레코드의 갯수
  filteredCount: 1000,
};
