import React, { PureComponent } from 'react';
import _ from 'lodash';
import { v4 } from 'uuid';
import UIContents from '../../Utils/UIContentsLoader';
const { TreeTraveser } = UIContents;

export default class Pane3 extends PureComponent {
  renderSelectBoxOptions(childNodes, parentid) {
    return _.map(childNodes, node => {
      const { model } = node;
      const modelData = JSON.stringify(model);
      return (
        <option
          key={v4()}
          value={model.id}
          data-model={modelData}
          data-parentid={parentid}
        >{model.name}</option>
      );
    });
  }

  renderRecords() {
    const { props } = this;
    const {
      QueryStateManagerState,
      AdvencedFilterArgManagerState,
    } = props;
    const { customDataTree } = AdvencedFilterArgManagerState;
    const { customDataTreeChildrenState } = QueryStateManagerState;
    const treeTraveser = new TreeTraveser(customDataTree);
    const rootNodes = treeTraveser.getGroupNodes();
    return _.map(rootNodes, node => {
      const { model, children } = node;
      const parentid = model.id;
      const dummyModel = {
        id: '0',
        name: '선택',
      };
      const selectedOption =
        _.find(customDataTreeChildrenState, data => data.parentid === parentid)
        || dummyModel;
      return (
        <tr key={v4()}>
          <th>{model.name}</th>
          <td>
            <label className="styled-select">
              <select
                className="form-control"
                defaultValue={selectedOption.id}
              >
                <option
                  value={dummyModel.id}
                  data-model={JSON.stringify(dummyModel)}
                  data-parentid={parentid}
                >{dummyModel.name}</option>
                {this.renderSelectBoxOptions(children, parentid)}
              </select>
            </label>
          </td>
        </tr>
      );
    });
  }

  render() {
    const { props } = this;
    return (
      <div role="tabpanel" className="tab-pane" id="searchFilterSet04">
        <h2 className="title1">고객 상담사 성향 데이터</h2>
        <div className="section-wrap">
          <h3 className="title2">커스텀 데이터 설정</h3>
          <div className="innerbox">
            <table className="filter-table">
              <tbody id={props.customDataPaneId}>
                {this.renderRecords()}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
