import {
  DetailPlayerManifest,
  DetailPlayerSampleDatas,
} from '../Components/DetailPlayer';
import {
  AdvencedFilterQuickViewerManifest,
  AdvencedFilterQuickViewerSampleDatas,
} from '../Components/AdvencedFilterQuickViewer';

export default class DashBoardLoader {
  get componentManifest() {
    const result = [
      DetailPlayerManifest,
      AdvencedFilterQuickViewerManifest,
    ];
    return result;
  }

  get componentSampleDatas() {
    const result = {
      DetailPlayerSampleDatas,
      AdvencedFilterQuickViewerSampleDatas,
    };
    return result;
  }
}
