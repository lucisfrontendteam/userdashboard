import React, { PureComponent } from 'react';
import $ from 'jquery';
import 'jquery-knob';
import _ from 'lodash';
import './custom.css';

export class Refresher extends PureComponent {
  static defaultProps = {
    id: 'timer',
    min: 0,
    max: 20,
  };

  constructor(props) {
    super(props);
    this.state = { pause: false };
  }

  get options() {
    const { props } = this;
    return {
      min: props.min,
      max: props.max,
      readOnly: true,
      fgColor: '#3c8dbc',
      width: 30,
      height: 30,
      thickness: '.2',
      draw() {
        $(this.i).css('font-size', '1em');
      },
    };
  }

  get sel() {
    const { props } = this;
    return `#${props.id}`;
  }

  get timeStamp() {
    return new Date().getTime();
  }

  handleFinish() {
    const { props } = this;
    props.refresherInitAction(this.timeStamp);
  }

  setTimer(callback) {
    clearTimeout(this.timer);
    this.timer = setInterval(() => {
      const value = $(this.sel).val();
      const nextValue = value - 1;
      $(this.sel).val(nextValue).trigger('change');
      if (nextValue === 0) {
        callback();
      }
    }, 1000);
  }

  installTimer() {
    $(this.sel).knob(this.options);
    this.setTimer(this.handleFinish.bind(this));
  }

  clearTimer() {
    clearTimeout(this.timer);
  }

  refreshTimer() {
    const currentVal = $(this.sel).val();
    const { props } = this;
    if (_.toInteger(currentVal) === props.min) {
      $(this.sel).val(props.max).trigger('change');
      this.setTimer(this.handleFinish.bind(this));
    }
  }

  updateTimerState() {
    this.setState({ pause: !!!this.state.pause });
    this.forceUpdate();
  }

  componentDidUpdate() {
    this.toggleTimer();
    this.refreshTimer();
  }

  componentDidMount() {
    this.installTimer();
  }

  componentWillUnmount() {
    this.clearTimer();
  }

  handlePause(e) {
    e.preventDefault();
    e.stopPropagation();
    this.updateTimerState();
  }

  toggleTimer() {
    const { state } = this;
    const $container = $(this.sel).parents('li');
    if (state.pause) {
      this.clearTimer();
      $container
        .find('span')
        .show();
      $container
        .find('input')
        .hide();
    } else {
      this.setTimer(this.handleFinish.bind(this));
      $container
        .find('span')
        .hide();
      $container
        .find('input')
        .show();
    }
  }

  render() {
    const { props } = this;
    return (
      <li
        className="dropdown basic-menu refresher"
        onClick={e => this.handlePause(e)}
      >
        <input
          type="text"
          id={props.id}
          defaultValue={props.max}
        />
        <span
          className="glyphicon glyphicon-pause pauser"
          aria-hidden="true"
        />
      </li>
    );
  }
}
