import React, { PureComponent } from 'react';
import { Router, Route, browserHistory, IndexRedirect } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import _ from 'lodash';
import { store } from '../Redux';
import { FailScenario } from './FailScenario';
import { ProjectPlayer } from '../Components/ProjectPlayer';
import {
  LoginPage,
  ProjectSelectPage,
} from '../Containers/Pages';
import PathManager from '../Utils/PathManager';

export class RouteScheduler extends PureComponent {
  get pathManager() {
    const { props } = this;
    return new PathManager(props);
  }

  handleGetComponent() {
    return (nextState, cb) => {
      const {
        scenarioRootUri: scenarioRootUriNext,
        scenarioUri: scenarioUriNext,
      } = nextState.params;
      const { props } = this;
      const { children } = props.ProjectState;
      const targetScenarioRoot = _.find(children, model => model.uri === scenarioRootUriNext);
      if (_.isUndefined(targetScenarioRoot)) {
        cb(null, FailScenario);
      } else {
        const targetScenario =
          _.find(targetScenarioRoot.children, model => model.uri === scenarioUriNext);
        if (_.isUndefined(targetScenario)) {
          cb(null, FailScenario);
        } else {
          props.scenarioSelectByUriAction(scenarioUriNext);
          cb(null, ProjectPlayer);
        }
      }
    };
  }

  handleOnRouteError() {
    const { pathManager } = this;
    browserHistory.push(pathManager.errorPath);
  }

  shouldComponentUpdate(nextProps) {
    const {
      LoginManagerState: nextUserState,
      ProjectState: nextProjectState,
    } = nextProps;
    const {
      LoginManagerState: currentUserState,
      ProjectState: currentProjectState,
    } = this.props;

    const userStateChanged
      = !!!_.isEqual(nextUserState, currentUserState);
    const projectStateChanged
      = !!!_.isEqual(nextProjectState, currentProjectState);
    return userStateChanged || projectStateChanged;
  }

  manageHistory() {
    const { props, pathManager } = this;
    const { ProjectState, LoginManagerState } = props;
    const projectNotFound = _.isEmpty(ProjectState);
    const loggedIn = LoginManagerState.status === true;
    if (projectNotFound) {
      if (loggedIn) {
        browserHistory.push(pathManager.projectSelectPath);
      } else {
        browserHistory.push(pathManager.loginPath);
      }
    } else {
      if (loggedIn) {
        browserHistory.push(pathManager.firstScenarioPathInfo.path);
      } else {
        browserHistory.push(pathManager.loginPath);
      }
    }
  }

  componentDidUpdate() {
    this.manageHistory();
  }

  componentDidMount() {
    this.manageHistory();
  }

  render() {
    const history = syncHistoryWithStore(browserHistory, store);
    const { pathManager } = this;
    return (
      <Router history={history} onError={this.handleOnRouteError} >
        <Route path="/" >
          <Route path={pathManager.rootPath} >
            <IndexRedirect to={pathManager.loginPath} />
            <Route path={pathManager.loginPath} component={LoginPage} />
            <Route path={pathManager.projectSelectPath} component={ProjectSelectPage} />
            <Route path={pathManager.errorPath} component={FailScenario} />
            <Route path={pathManager.projectRootPath}>
              <Route
                path="/:rootUri/:projectRootUri/:scenarioRootUri/:scenarioUri"
                getComponent={this.handleGetComponent()}
              />
            </Route>
          </Route>
        </Route>
        <Route status="404" path="*" component={FailScenario} />
      </Router>
    );
  }
}
