import React, { PureComponent } from 'react';

export default class Navigation extends PureComponent {
  render() {
    return (
      <div className="filter-navi">
        <h2 className="title1">검색 목록</h2>
        <ul className="nav nav-tabs" role="tablist">
          <li role="presentation" className="active">
            <a href="#searchFilterSet01" role="tab" data-toggle="tab">조직 / 상담</a>
          </li>
          <li role="presentation">
            <a href="#searchFilterSet02" role="tab" data-toggle="tab">날짜 / 기간</a>
          </li>
          <li role="presentation">
            <a href="#searchFilterSet03" role="tab" data-toggle="tab">통화데이터</a>
          </li>
          <li role="presentation" style={{ display: 'none' }}>
            <a href="#searchFilterSet04" role="tab" data-toggle="tab">고객상담사 성향데이터</a>
          </li>
          <li role="presentation">
            <a href="#searchFilterSet05" role="tab" data-toggle="tab">카테고리</a>
          </li>
        </ul>
      </div>
    );
  }
}
