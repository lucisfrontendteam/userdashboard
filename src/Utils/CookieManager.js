import cookie from 'react-cookie';
import moment from 'moment';

export default class CookieManager {
  static saveCookie(query = {}, cookieKey) {
    cookie.save(cookieKey, query, {
      path: '/',
      expires: moment().add(365, 'days').toDate(),
    });
  }

  static clearCookie(cookieKey) {
    CookieManager.saveCookie({}, cookieKey);
  }
}
