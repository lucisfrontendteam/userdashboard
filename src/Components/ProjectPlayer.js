import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { actions } from '../Redux';
import '../Resources/css/jquery-ui.css';
import '../Resources/css/jquery.timepicker.css';
import '../Resources/css/jquery.dataTables.css';
import '../Resources/css/jquery.mCustomScrollbar.css';
import '../Resources/css/jquery.jqtransform.css';
import '../Resources/css/jqtree.css';
import '../Resources/css/bootstrap.css';
import '../Resources/css/bootstrap-datepicker.standalone.css';
import '../Resources/css/lucis-base.css';
import '../Resources/css/lucis-custom.css';
import '../Resources/css/lucis-widget.css';
import '../Resources/css/lucis-theme.css';
import $ from 'jquery';
import _ from 'lodash';
import 'bootstrap';
import { Scenario } from '../Containers/Scenario';
import AdvencedFilter from './AdvencedFilter';
import { DetailPlayer } from './DetailPlayer';
import UIContents from '../Utils/UIContentsLoader';
const { PropsCreator } = UIContents;

class ProjectPlayer extends PureComponent {
  constructor(props) {
    super(props);
    this.initOldIeSupport();
    this.state = { timestamp: new Date().getTime() };
  }

  initOldIeSupport() {
    // const ie9TrapFound = document.getElementById('ie9Trap');
    // if (ie9TrapFound) {
    //   // eslint-disable-next-line
    //   require('../Resources/css/lucis-ie9.css');
    // }

    const ie8TrapFound = document.getElementById('ie8Trap');
    if (ie8TrapFound) {
      // eslint-disable-next-line
      require('html5shiv');
      // eslint-disable-next-line
      require('../Resources/js/respond.min.js');
    }
  }

  shouldComponentUpdate(nextProps) {
    const {
      LoginManagerState: nextUserState,
      ProjectState: nextProjectState,
      QueryStateManagerState: nextQueryState,
      AdvencedFilterArgManagerState: nextAAS,
      DetailPlayerState: nextDetailPlayerState,
      ExtraQueryState: nextExtraQueryState,
      RefresherState: nextRefresherState,
      routeParams: nextrouteParams,
    } = nextProps;
    const {
      LoginManagerState: currentUserState,
      ProjectState: currentProjectState,
      QueryStateManagerState: currentQueryState,
      AdvencedFilterArgManagerState: prevAAS,
      DetailPlayerState: currentDetailPlayerState,
      ExtraQueryState: currentExtraQueryState,
      RefresherState: currentRefresherState,
      routeParams: currentrouteParams,
    } = this.props;

    const userStateChanged = !!!_.isEqual(nextUserState, currentUserState);
    const pageChanged =
      !!!_.isEqual(nextrouteParams.scenarioUri, currentrouteParams.scenarioUri);
    const projectStateChanged = !!!_.isEqual(nextProjectState.id, currentProjectState.id);
    const queryChanged = !!!_.isEqual(nextQueryState, currentQueryState);
    const extraQueryChanged = !!!_.isEqual(nextExtraQueryState, currentExtraQueryState);
    const detailPlayerChanged = !!!_.isEqual(nextDetailPlayerState, currentDetailPlayerState);
    const aasChanged = !!!_.isEqual(nextAAS, prevAAS);
    const refresherChanged = !!!_.isEqual(nextRefresherState, currentRefresherState);

    const reload = (
      userStateChanged || pageChanged ||
      projectStateChanged || queryChanged ||
      detailPlayerChanged || extraQueryChanged || aasChanged ||
      refresherChanged
    );

    // if (reload) {
    //   console.log('userStateChanged', userStateChanged);
    //   console.log('pageChanged', pageChanged);
    //   console.log('projectStateChanged', projectStateChanged);
    //   // console.log('queryChanged', queryChanged);
    //   console.log('detailPlayerChanged', detailPlayerChanged);
    //   console.log('extraQueryChanged', extraQueryChanged);
    // }

    return reload;
  }

  get logggedInAndScenarioRouted() {
    const { props } = this;
    const { LoginManagerState, routeParams } = props;
    const loggedIn = LoginManagerState.status === true;
    const { scenarioUri } = routeParams;
    const routed = !!!_.isEmpty(scenarioUri);
    return loggedIn && routed;
  }

  updateQueryState(prevProps = {}) {
    const { props } = this;
    const { QueryStateManagerState: currentQueryState } = props;
    const { QueryStateManagerState: prevQueryState } = prevProps;
    const queryStateChanged = !!!_.isEqual(currentQueryState, prevQueryState);
    if (this.logggedInAndScenarioRouted && queryStateChanged) {
      const newProps = PropsCreator.getAddtionalProps(props);
      props.querystatemanagerreadInitAction(newProps);
    }
  }

  updateAdvencedFilterArguments() {
    if (this.logggedInAndScenarioRouted) {
      const { props } = this;
      const newProps = PropsCreator.getAddtionalProps(props);
      props.advencedfilterargmanagerreadInitAction(newProps);
    }
  }

  updateProjectTheme() {
    const { props } = this;
    const { ProjectState } = props;
    const projectClassName = ProjectState.projectThemeId;
    $('#root').removeClass().addClass(projectClassName);
  }

  componentDidMount() {
    this.updateProjectTheme();
    this.updateQueryState();
    this.updateAdvencedFilterArguments();
  }

  componentWillUnmount() {
    const { props } = this;
    props.projectInitAction(undefined, props);
  }

  render() {
    const { props } = this;
    return (
      <div>
        <div className="wrapper">
          <div className="main-bg">
            <span className="lt" />
            <span className="lb" />
            <span className="rt" />
            <span className="rb" />
          </div>
          <Scenario {...props} />
        </div>
        <AdvencedFilter {...props} />
        <DetailPlayer {...props} />
      </div>
    );
  }
}

const mapStateToProps = (state) => state;
const connectedProjectPlayer = connect(mapStateToProps, actions)(ProjectPlayer);
export { connectedProjectPlayer as ProjectPlayer };
