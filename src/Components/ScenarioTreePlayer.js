import React, { PureComponent } from 'react';
import _ from 'lodash';
import { ScenarioTree } from './ScenarioTree';
import UIContnets from '../Utils/UIContentsLoader';
const { TreeTraveser } = UIContnets;

export class ScenarioTreePlayer extends PureComponent {
  render() {
    const { props } = this;
    const treeTraveser = new TreeTraveser(props.ProjectState);
    const { accessPath } = treeTraveser.getRootModel();
    const depth1Models = treeTraveser.getNodeListByPredict(node => {
      const isRoot = node.getPath().length === 2;
      const isEmptyRoot = _.isEmpty(node.children);
      return isRoot && !!!isEmptyRoot;
    });
    return (
      <div className="gnb">
        <ScenarioTree {...props} accessPath={accessPath} depth1Models={depth1Models} />
      </div>
    );
  }
}
