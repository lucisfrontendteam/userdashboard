import * as actionTypes from './actionTypes';
import DetailPlayerSampleDatas from '../DetailPlayerSampleDatas';
import UIContents from '../../../Utils/UIContentsLoader';
const { AjaxManager } = UIContents;
import manifest from '../manifest';

export function detailplayerInitAction(props) {
  return dispatch => {
    const { NetWorkModeState, QueryIdMap } = props;
    const { apiUrl, method } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.DETAIL_PLAYER_INIT,
      query: QueryIdMap,
      ajaxType: 'normal',
      sampleData: { ...DetailPlayerSampleDatas },
    };
    AjaxManager.run(param);
  };
}

export function detailplayerClearAction() {
  return {
    type: actionTypes.DETAIL_PLAYER_CLEAR,
  };
}
