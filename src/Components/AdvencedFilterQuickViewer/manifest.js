export default {
  id: 'AdvencedFilterQuickViewer',
  stateId: 'AdvencedFilterQuickViewer',
  name: '필터결과 현황',
  usage: '읽기',
  type: 'IdList',
  inputIdList: [
    {
      key: 'userId',
      id: 'advencedFilterQuickViewerUserId',
    },
    {
      key: 'projectId',
      id: 'advencedFilterQuickViewerProjectId',
    },
    {
      key: 'scenarioId',
      id: 'advencedFilterQuickViewerScenarioId',
    },
    {
      key: 'tabId',
      id: 'advencedFilterQuickViewerTabId',
    },
  ],
  method: 'post',
  apiUrl: '/project/resultSetTotalCount.api',
};
