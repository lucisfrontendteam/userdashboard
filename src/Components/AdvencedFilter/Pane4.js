import React, { PureComponent } from 'react';
import _ from 'lodash';
import $ from 'jquery';
import 'jqtree';
import AdvencedFilterUtil from './AdvencedFilterUtil';

export default class Pane4 extends PureComponent {
  get categoryNodeList() {
    const { props } = this;
    const { categoryFilterTree } = props.AdvencedFilterArgManagerState;
    const {
      categoryFilterListState = [],
    } = props.QueryStateManagerState;
    const nodeList
      = AdvencedFilterUtil.getNodeListFromOriginalTree(
      categoryFilterListState, categoryFilterTree
    );
    return nodeList;
  }

  get categotyGroupNodeList() {
    const { props } = this;
    const { categoryFilterTree } = props.AdvencedFilterArgManagerState;
    const {
      categoryGroupFilterListState = [],
    } = props.QueryStateManagerState;
    const nodeList
      = AdvencedFilterUtil.getNodeListFromOriginalTree(
      categoryGroupFilterListState, categoryFilterTree
    );
    return nodeList;
  }

  initializeTrees() {
    const { props, categotyGroupNodeList, categoryNodeList } = this;
    const { categoryFilterTree } = props.AdvencedFilterArgManagerState;

    this.$categoryTree = $('#tree3-1').tree({
      data: categoryFilterTree,
      autoOpen: true,
      buttonLeft: false,
    });

    this.$categoryGroupFilterList
      = $(document.getElementById(props.targetCategoryGroupListId)).tree({
        data: categotyGroupNodeList,
        buttonLeft: false,
      });

    this.$categoryFilterList
      = $(document.getElementById(props.targetCategoryListId)).tree({
        data: categoryNodeList,
        buttonLeft: false,
      });
  }

  componentWillUnmount() {
    this.$categoryTree.tree('destroy');
    this.$categoryGroupFilterList.tree('destroy');
    this.$categoryFilterList.tree('destroy');
  }

  handleCategoryAddOnClick() {
    const { $categoryTree } = this;
    const selectedNode = $categoryTree.tree('getSelectedNode');
    if (selectedNode) {
      const isParentNode = selectedNode.type === 'group';
      if (isParentNode) {
        this.addSelectedCategoryGroupNode(selectedNode);
      } else {
        this.addSelectedCategoryNode(selectedNode);
      }
    }
  }

  addSelectedCategoryGroupNode(selectedCategoryGroupNode) {
    const { $categoryGroupFilterList } = this;
    const rootNode = {
      ...selectedCategoryGroupNode,
      children: undefined,
    };
    this.addSelectedNode(
      $categoryGroupFilterList,
      rootNode
    );
  }

  addSelectedCategoryNode(selectedCategoryNode) {
    const { $categoryFilterList } = this;
    this.addSelectedNode(
      $categoryFilterList,
      selectedCategoryNode
    );
  }

  addSelectedNode($targetTree, targetNode) {
    const listState = JSON.parse($targetTree.tree('toJson'));
    const dupNode = _.find(listState, node => node.id === targetNode.id);
    if (_.isUndefined(dupNode)) {
      const rootNode = $targetTree.tree('getTree');
      $targetTree.tree('appendNode', targetNode, rootNode);
    }
  }

  handleCategoryRemoveOnClick() {
    const {
      $categoryFilterList,
      $categoryGroupFilterList,
    } = this;
    const selectedCategoryNode = $categoryFilterList.tree('getSelectedNode');
    $categoryFilterList.tree('removeNode', selectedCategoryNode);

    const selectedCategoryGroupNode = $categoryGroupFilterList.tree('getSelectedNode');
    $categoryGroupFilterList.tree('removeNode', selectedCategoryGroupNode);
  }

  destroyTrees() {
    this.$categoryTree.tree('destroy');
    this.$categoryGroupFilterList.tree('destroy');
    this.$categoryFilterList.tree('destroy');
  }

  componentDidMount() {
    this.initializeTrees();
  }

  componentDidUpdate() {
    this.destroyTrees();
    this.initializeTrees();
  }

  render() {
    const { props } = this;
    return (
      <div role="tabpanel" className="tab-pane" id="searchFilterSet05">
        <h2 className="title1">카테고리</h2>
        <div className="row select-ui">
          <div className="col-md-6 list-left">
            <div className="section-wrap">
              <h3 className="title2">카테고리 목록</h3>
              <div className="innerbox tree-list scroll-y" id="tree3-1" />
            </div>
          </div>
          <div className="col-md-6 list-right">
            <div className="row">
              <div className="col-md-12">
                <div className="section-wrap">
                  <h3 className="title2">카테고리 그룹</h3>
                  <div
                    className="innerbox tree-list scroll-y small"
                    id={props.targetCategoryGroupListId}
                  />
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12">
                <div className="section-wrap">
                  <h3 className="title2">카테고리</h3>
                  <div
                    className="innerbox tree-list scroll-y small"
                    id={props.targetCategoryListId}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="btn-area">
            <button
              type="button"
              className="btn-img-select right"
              onClick={() => this.handleCategoryAddOnClick()}
            >추가</button>
            <button
              type="button"
              className="btn-img-select left"
              onClick={() => this.handleCategoryRemoveOnClick()}
            >삭제</button>
          </div>
        </div>
      </div>
    );
  }
}
