import React, { PureComponent } from 'react';
import _ from 'lodash';
import UIContents from '../Utils/UIContentsLoader';
import { connect } from 'react-redux';
import { actions } from '../Redux';
import { GnbPlayer } from '../Components/GnbPlayer';
const { PropsCreator, WidgetViewer } = UIContents;

class Scenario extends PureComponent {
  pageChanged(prevProps) {
    const { routeParams: currentrouteParams = {} } = this.props;
    const { routeParams: prevrouteParams = {} } = prevProps;
    const pageChanged =
      !!!_.isEqual(currentrouteParams.scenarioUri, prevrouteParams.scenarioUri);
    return pageChanged;
  }

  get logggedInAndScenarioRouted() {
    const { props } = this;
    const { LoginManagerState, routeParams } = props;
    const loggedIn = LoginManagerState.status === true;
    const { scenarioUri } = routeParams;
    const routed = !!!_.isEmpty(scenarioUri);
    return loggedIn && routed;
  }

  updateQueryState(prevProps = {}) {
    if (this.logggedInAndScenarioRouted && this.pageChanged(prevProps)) {
      const { props } = this;
      const newProps = PropsCreator.getAddtionalProps(props);
      props.querystatemanagerreadInitAction(newProps);
    }
  }

  updateAdvencedFilterArguments(prevProps = {}) {
    if (this.logggedInAndScenarioRouted && this.pageChanged(prevProps)) {
      const { props } = this;
      const newProps = PropsCreator.getAddtionalProps(props);
      props.advencedfilterargmanagerreadInitAction(newProps);
    }
  }

  componentDidUpdate(prevProps) {
    this.updateQueryState(prevProps);
    this.updateAdvencedFilterArguments(prevProps);
  }

  render() {
    const { props } = this;
    return (
      <div>
        <GnbPlayer {...props} />
        <WidgetViewer {...props} />
      </div>
    );
  }
}

const mapStateToProps = (state) => state;
const connectedScenario = connect(mapStateToProps, actions)(Scenario);
export { connectedScenario as Scenario };
