import React, { PureComponent } from 'react';

export class NetworkSwitcher extends PureComponent {
  handleToggleNetwork() {
    const { props } = this;
    const { value: on } = props.NetWorkModeState;
    if (on) {
      props.networkModeOffAction();
    } else {
      props.networkModeOnAction();
    }
  }

  render() {
    const { props } = this;
    const { value: on } = props.NetWorkModeState;
    let className = 'glyphicon glyphicon-phone-alt';
    if (on) {
      className = 'glyphicon glyphicon-earphone';
    }
    return (
      <li className="dropdown basic-menu">
        <span
          id="networkSwitch"
          style={{ cursor: 'pointer' }}
          className={className}
          aria-hidden="true"
          onClick={() => this.handleToggleNetwork()}
        />
      </li>
    );
  }
}
