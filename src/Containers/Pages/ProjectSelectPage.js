import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import { v4 } from 'uuid';
import { browserHistory } from 'react-router';
import { actions } from '../../Redux';
import '../../Resources/css/jquery-ui.css';
import '../../Resources/css/jquery.timepicker.css';
import '../../Resources/css/jquery.dataTables.css';
import '../../Resources/css/jquery.mCustomScrollbar.css';
import '../../Resources/css/jquery.jqtransform.css';
import '../../Resources/css/jqtree.css';
import '../../Resources/css/bootstrap.css';
import '../../Resources/css/bootstrap-datepicker.standalone.css';
import '../../Resources/css/lucis-base.css';
import '../../Resources/css/lucis-custom.css';
import '../../Resources/css/hover.css';
import UIContents from '../../Utils/UIContentsLoader';
const {
  TextRenderer,
  EnvironmentManager,
} = UIContents;
import environment from '../../../config/environment';

class ProjectSelectPage extends PureComponent {
  constructor(props) {
    super(props);
    this.initOldIeSupport();
  }

  initOldIeSupport() {
    // const ie9TrapFound = document.getElementById('ie9Trap');
    // if (ie9TrapFound) {
    //   // eslint-disable-next-line
    //   require('../Resources/css/admin-ie9.css');
    // }

    const ie8TrapFound = document.getElementById('ie8Trap');
    if (ie8TrapFound) {
      // eslint-disable-next-line
      require('html5shiv');
      // eslint-disable-next-line
      require('../../Resources/js/respond.min.js');
    }
  }

  handleProjectSelect(id, props) {
    props.projectInitAction(id, props);
  }

  renderRecords(projectList) {
    const records = _.map(projectList, project => {
      const {
        thumbnail,
        siteName,
        createdAt,
        owner,
        id,
      } = project;
      const { props } = this;
      const createdDate = TextRenderer.renderDateTimeString(
        createdAt, 'YYYYMMDD', 'YYYY.MM.DD'
      );
      return (
        <div
          onClick={() => this.handleProjectSelect(id, props)}
          className="project-list"
          key={v4()}
          style={{ cursor: 'pointer' }}
        >
          <div className="thumbnail-area hvr-curl-top-right">
            <label className="input-file-label">
              <div className="img-area">
                <img src={thumbnail} alt="" />
              </div>
            </label>
          </div>
          <div className="info-area">
            <dl>
              <dt>사이트명:</dt>
              <dd>{siteName}</dd>
            </dl>
            <dl>
              <dt>생성일시:</dt>
              <dd>{createdDate}</dd>
            </dl>
            <dl>
              <dt>Owner:</dt>
              <dd>{owner}</dd>
            </dl>
          </div>
        </div>
      );
    });
    return records;
  }

  renderRows() {
    const { props } = this;
    const { ProjectManagerState } = props;
    const stateChunks = _.chunk(ProjectManagerState, 6);
    return _.map(stateChunks, projectList => {
      const records = projectList;
      return (
        <tr key={v4()}>
          <td>
            <div className="project-list-wrap">
              {this.renderRecords(records)}
            </div>
          </td>
        </tr>
      );
    });
  }

  componentDidMount() {
    const { props } = this;
    if (props.LoginManagerState.status !== true) {
      const { errorPath } = new EnvironmentManager(environment);
      browserHistory.push(errorPath);
    } else {
      props.projectmanagerreadInitAction(props);
    }
  }

  render() {
    return (
      <div className="wrapper">
        <div className="project-list-wrapper">
          <div className="project-list-body">
            <div className="project-list-title">
              <h1>프로젝트 선택</h1>
            </div>
            <table className="project-list-layout">
              <tbody>
                {this.renderRows()}
              </tbody>
            </table>
          </div>
          <div className="project-list-foot" />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => state;
const connectedProjectSelectPage = connect(mapStateToProps, actions)(ProjectSelectPage);
export { connectedProjectSelectPage as ProjectSelectPage };
