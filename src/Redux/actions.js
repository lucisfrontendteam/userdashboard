import _ from 'lodash';
import * as actionTypes from './actionTypes';
import DefaultStates from './DefaultStates';
const defaultStates = new DefaultStates();
import UIContentsLoader from '../Utils/UIContentsLoader';
const {
  WidgetRefiner,
  WidgetCores,
  CommonManagerActionList,
} = UIContentsLoader;
const { widgetActionList } = new WidgetRefiner(WidgetCores);
import { DetailPlayerActions } from '../Components/DetailPlayer';
import { AdvencedFilterQuickViewerActions } from '../Components/AdvencedFilterQuickViewer';
import {
  NetworkSwitcherActions,
} from '../Components/NetworkSwitcher';

function refresherInitAction(timeStamp) {
  return {
    type: actionTypes.REFRESHER_INIT,
    payload: { timeStamp },
  };
}

function widgetCoreListInitAction(widgetCoreListState = defaultStates.widgetCoreListState) {
  return {
    type: actionTypes.WIDGET_CORE_LIST_INIT,
    payload: { widgetCoreListState },
  };
}

function widgetThemeListInitAction(widgetThemeListState = defaultStates.widgetThemeState) {
  return {
    type: actionTypes.WIDGET_THEME_LIST_INIT,
    payload: { widgetThemeListState },
  };
}

function widgetViewerThemeListInitAction(
  widgetViewerThemeListState = defaultStates.widgetViewerThemeListState
) {
  return {
    type: actionTypes.WIDGET_VIEWER_THEME_LIST_INIT,
    payload: { widgetViewerThemeListState },
  };
}

function projectThemeListInitAction(
  projectThemeListState = defaultStates.projectThemeListState,
) {
  return {
    type: actionTypes.PROJECT_THEME_LIST_INIT,
    payload: { projectThemeListState },
  };
}

function projectInitAction(projectId, props) {
  const { ProjectManagerState } = props;
  const selectedProject =
    _.find(ProjectManagerState, projectState => projectState.id === projectId);
  return {
    type: actionTypes.PROJECT_INIT,
    payload: { projectState: selectedProject },
  };
}

function scenarioSelectByUriAction(scenarioUri) {
  return {
    type: actionTypes.SCENARIO_SELECT,
    payload: { scenarioUri },
  };
}

function organizationTreeViewTypeInitAction(
  viewType = defaultStates.organizationTreeViewTypeState
) {
  return {
    type: actionTypes.ORGANIZATION_TREE_VIEW_TYPE_INIT,
    payload: { viewType },
  };
}

function organizationTreeSearchKeywordInitAction(keyword) {
  return {
    type: actionTypes.ORGANIZATION_TREE_SEARCH_KEYWORD_INIT,
    payload: { keyword },
  };
}

export function extraqueryInitAction() {
  return {
    type: actionTypes.EXTRA_QUERY_INIT,
    payload: {
      CategoryGroupFilterListState: {},
    },
  };
}

export function extraCategory1FilterListStateInitAction(id) {
  const CategoryFilterListStateId = _.toString(id);
  return {
    type: actionTypes.EXTRA_CATEGORYFILTERLISTSTATE_INIT,
    payload: { CategoryFilterListStateId },
  };
}

export const actions = {
  ...widgetActionList,
  ...DetailPlayerActions,
  ...CommonManagerActionList,
  ...AdvencedFilterQuickViewerActions,
  ...NetworkSwitcherActions,
  widgetCoreListInitAction,
  widgetThemeListInitAction,
  widgetViewerThemeListInitAction,
  projectThemeListInitAction,
  projectInitAction,
  scenarioSelectByUriAction,
  organizationTreeViewTypeInitAction,
  organizationTreeSearchKeywordInitAction,
  extraqueryInitAction,
  extraCategory1FilterListStateInitAction,
  refresherInitAction,
};
