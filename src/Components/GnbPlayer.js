import React, { PureComponent } from 'react';
import { ScenarioTreePlayer } from './ScenarioTreePlayer';
import { LoginStatusViewer } from './LoginStatusViewer';
import { AdvencedFilterQuickViewer } from './AdvencedFilterQuickViewer';
import VersionViewer from './VersionViewer';
import { CIImageViewer } from './CIImageViewer';
import { Refresher } from './Refresher';
import { ForceRefresher } from './ForceRefresher';

export class GnbPlayer extends PureComponent {
  render() {
    const { props } = this;
    return (
      <header className="main-header">
        <div className="main-header-inner">
          <CIImageViewer {...props} />
          <nav className="gnb-area">
            <div className="top-menu">
              <VersionViewer {...props} />
              <ul>
                <Refresher {...props} />
                <ForceRefresher {...props} />
                <AdvencedFilterQuickViewer {...props} />
                <LoginStatusViewer {...props} />
              </ul>
            </div>
            <ScenarioTreePlayer {...props} />
          </nav>
        </div>
      </header>
    );
  }
}
