import { combineReducers } from 'redux';
import WidgetCoreListState from './WidgetCoreListReducer';
import WidgetThemeListState from './WidgetThemeListReducer';
import WidgetViewerLayoutListState from './WidgetViewerLayoutListReducer';
import ProjectState from './ProjectReducer';
import ProjectThemeListState from './ProjectThemeListReducer';
import OrganizationTreeViewTypeState from './OrganizationTreeViewTypeReducer';
import OrganizationTreeSearchKeywordState from './OrganizationTreeSearchKeywordReducer';
import { routerReducer } from 'react-router-redux';
import ExtraQueryState from './ExtraQueryReducer';
import RefresherState from './RefresherReducer';
import UIContentsLoader from '../../Utils/UIContentsLoader';
const {
  WidgetRefiner,
  WidgetCores,
  CommonManagerReducers,
} = UIContentsLoader;
const { widgetReducers } = new WidgetRefiner(WidgetCores);
import { DetailPlayerReducer } from '../../Components/DetailPlayer';
import { AdvencedFilterQuickViewerReducer } from '../../Components/AdvencedFilterQuickViewer';
import { NetworkSwitcherReducer } from '../../Components/NetworkSwitcher';

export const dashboardReducers = {
  ...widgetReducers,
  ...DetailPlayerReducer,
  ...CommonManagerReducers,
  ...AdvencedFilterQuickViewerReducer,
  ...NetworkSwitcherReducer,
  WidgetCoreListState,
  WidgetThemeListState,
  WidgetViewerLayoutListState,
  ProjectState,
  ProjectThemeListState,
  OrganizationTreeViewTypeState,
  OrganizationTreeSearchKeywordState,
  ExtraQueryState,
  RefresherState,
  routing: routerReducer,
};

export default combineReducers(dashboardReducers);
