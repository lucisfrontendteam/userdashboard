import React, { PureComponent } from 'react';
import './custom.css';

export class ForceRefresher extends PureComponent {
  get timeStamp() {
    return new Date().getTime();
  }

  handleRefresh(e) {
    e.preventDefault();
    e.stopPropagation();
    const { props } = this;
    props.refresherInitAction(this.timeStamp);
  }

  render() {
    return (
      <li
        className="dropdown basic-menu"
        onClick={e => this.handleRefresh(e)}
      >
        <span
          className="glyphicon glyphicon-refresh forceRefresher"
          aria-hidden="true"
        />
      </li>
    );
  }
}
