import * as actionTypes from '../actionTypes';

export default function OrganizationTreeSearchKeywordState(state = '', action = null) {
  let finalState = '';
  switch (action.type) {
    case actionTypes.ORGANIZATION_TREE_SEARCH_KEYWORD_INIT: {
      const { keyword } = action.payload;
      finalState = keyword;
    } break;

    default: {
      finalState = state;
    }
  }

  return finalState;
}
