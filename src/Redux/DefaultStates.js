import _ from 'lodash';
import moment from 'moment';
import UIContents from '../Utils/UIContentsLoader';
const {
  WidgetCores,
  WidgetThemes,
  WidgetViewerLayouts,
  WidgetRefiner,
  ProjectManagerReadSampleDatas,
  AdvencedFilterArgManagerReadSampleDatas,
} = UIContents;
const { widgetCores } = new WidgetRefiner(WidgetCores);

export default class DefaultStates {
  static queryKey = 'advencedFilter';
  static emptyOption = { id: 0, name: '선택' };
  static zeroTime = '0000';

  get widgetCoreListState() {
    return _.map(widgetCores, widgetCore => {
      const { displayName, element } = widgetCore;
      const { name, type } = element.defaultProps;
      return {
        constructor: element,
        displayName,
        name,
        type,
      };
    });
  }

  get widgetThemeState() {
    return _.map(Object.keys(WidgetThemes), displayName => {
      const constructor = WidgetThemes[displayName];
      const { name, type } = constructor.defaultProps;
      return {
        constructor,
        displayName,
        name,
        type,
      };
    });
  }

  get widgetViewerThemeListState() {
    return _.map(Object.keys(WidgetViewerLayouts), displayName => {
      const constructor = WidgetViewerLayouts[displayName];
      const { name, type } = constructor.defaultProps;
      return {
        constructor,
        displayName,
        name,
        type,
      };
    });
  }

  get ProjectManagerState() {
    return ProjectManagerReadSampleDatas;
  }

  get organizationTreeViewTypeState() {
    return 'tree';
  }

  get AdvencedFilterArgManagerState() {
    return AdvencedFilterArgManagerReadSampleDatas;
  }

  // 선택된 날짜 / 기간 타입
  get targetDateRangeType() {
    return 'ALL';
  }

  // 최근일
  get recentDays() {
    return 7;
  }

  // 조회 시작일
  get dateFrom() {
    return moment().add(-7, 'days').format('YYYYMMDD');
  }

  // 조회 종료일
  get dateTo() {
    return moment().format('YYYYMMDD');
  }

  // // 루트 계정
  // get rootUser() {
  //   return {
  //     userId: 'root',
  //     userPass: 'root',
  //   };
  // }
  //
  // // LoginManagerState 초기값
  // get userState() {
  //   return {
  //     // 로그인 여부
  //     status: false,
  //     statusText: '',
  //     // 에러메세지 표시여부
  //     showError: false,
  //     userId: '',
  //     userPass: '',
  //   };
  // }
}
