import React, { PureComponent } from 'react';
import Pane2SelectBox from './Pane2SelectBox';
import './Pane2.css';

export default class Pane2 extends PureComponent {
  static defaultProps = {
    filterTypes: {
      SILENCE: 'SILENCE',
      TELTIME: 'TELTIME',
      HO: 'HO',
      SCSPEECH: 'SCSPEECH',
      CLIENTSPEECH: 'CLIENTSPEECH',
      DUPS: 'DUPS',
    },
  };

  render() {
    const { props } = this;
    const { filterTypes } = props;
    return (
      <div role="tabpanel" className="tab-pane" id="searchFilterSet03">
        <h2 className="title1">통화 데이터</h2>
        <div className="section-wrap">
          <h3 className="title2">통화 데이터 설정</h3>
          <div className="innerbox">
            <table className="filter-table">
              <tbody>
                <tr>
                  <th>통화시간</th>
                  <td>
                    <label className="styled-select">
                      <Pane2SelectBox {...props} type={filterTypes.TELTIME} />
                    </label>
                  </td>
                </tr>
                <tr>
                  <th>묵음비율</th>
                  <td>
                    <label className="styled-select">
                      <Pane2SelectBox {...props} type={filterTypes.SILENCE} />
                    </label>
                  </td>
                </tr>
                <tr>
                  <th>호 방향</th>
                  <td>
                    <label className="styled-select">
                      <Pane2SelectBox {...props} type={filterTypes.HO} />
                    </label>
                  </td>
                </tr>
                <tr className="hide">
                  <th>상담사 발화비율</th>
                  <td>
                    <label className="styled-select" />
                  </td>
                </tr>
                <tr className="hide">
                  <th>고객 발화비율</th>
                  <td>
                    <label className="styled-select" />
                  </td>
                </tr>
                <tr className="hide">
                  <th>말겹침 비율</th>
                  <td>
                    <label className="styled-select" />
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
