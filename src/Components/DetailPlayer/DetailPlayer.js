import React, { PureComponent } from 'react';
import _ from 'lodash';
import UIContentsLoader from '../../Utils/UIContentsLoader';
const {
  BasicDataTable,
  WidgetDefaultProps,
  WidgetRenderer,
} = UIContentsLoader;
import DetailPlayerDataAdapter from './DetailPlayerDataAdapter';
import DetailPlayerDefaultOptions from './DetailPlayerDefaultOptions';
import manifest from './manifest';
import ReactPaginate from 'react-paginate';

export class DetailPlayer extends PureComponent {
  static defaultProps = {
    ...manifest,
  };

  constructor(props) {
    super(props);
    this.shouldComponentUpdate = WidgetDefaultProps.shouldComponentUpdate.bind(this);
    this.firePagingAction = WidgetDefaultProps.firePagingAction.bind(this);
    this.handlePagingClick = WidgetDefaultProps.handlePagingClick.bind(this);
    this.pagingProps = WidgetDefaultProps.getPagingProps();
    this.state = { pageNumber: 1 };
  }

  toggleDetailPlayer() {
    const { props } = this;
    const { DetailPlayerState } = props;
    if (!!!_.isEmpty(DetailPlayerState)) {
      window.$('#listenListLayer').modal('show');
    } else {
      window.$('#listenListLayer').modal('hide');
    }
  }

  clearDetailPlayer() {
    const { detailplayerClearAction } = this.props;
    if (detailplayerClearAction) {
      detailplayerClearAction();
    }
  }

  componentDidUpdate() {
    this.toggleDetailPlayer();
  }

  componentDidMount() {
    this.toggleDetailPlayer();
    window.$('#listenListLayer').on('hidden.bs.modal', () => {
      this.clearDetailPlayer();
    });
  }

  componentWillUnmount() {
    this.clearDetailPlayer();
  }

  renderWidgetWrapper(children) {
    return (
      <div className="modal fade" tabIndex="-1" role="dialog" id="listenListLayer">
        <div className="modal-dialog" role="document" style={{ width: '1100px' }}>
          <section className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <h1 className="modal-title" id="gridSystemModalLabel">청취 목록</h1>
            </div>
            <div className="modal-body">
              {children}
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-default btn-modal"
                data-dismiss="modal"
              >닫기</button>
            </div>
          </section>
        </div>
      </div>
    );
  }

  renderWidgetCore(widgetState) {
    const { props } = this;
    const adapter = new DetailPlayerDataAdapter(
      widgetState,
      DetailPlayerDefaultOptions
    );
    let core = {};
    if (_.isEmpty(adapter.errors)) {
      const { defaultData } = adapter;
      core = (
        <div className="row">
          <div className="col-md-12">
            <div className="lucis-flip-container">
              <div className="lucis-flipper">
                <div className="front">
                  <div className="lucis-widget">
                    <h2 className="tbl-title">테이블 타이틀</h2>
                    <BasicDataTable
                      {...props}
                      options={defaultData.grid.options}
                      datas={defaultData.grid.datas}
                    />
                    <div className="clearfix">
                      <ReactPaginate
                        pageNum={defaultData.grid.totalPageNumber}
                        clickCallback={this.handlePagingClick}
                        {...this.pagingProps}
                      />
                    </div>
                  </div>
                </div>
                <div className="back" />
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      core = WidgetRenderer.renderDataFailWidgetCore(true, adapter.errors);
    }
    return core;
  }

  render() {
    const { props } = this;
    const { id } = this.constructor.defaultProps;
    const stateName = `${id}State`;
    const TargetState = props[stateName];
    let widget = {};
    if (_.isEmpty(TargetState)) {
      widget = WidgetRenderer.renderLoadingWidgetCore(false);
    } else if (TargetState.err) {
      widget = WidgetRenderer.renderNetworkFailWidgetCore(TargetState, false);
    } else {
      widget = this.renderWidgetCore(TargetState);
    }
    return this.renderWidgetWrapper(widget);
  }
}
