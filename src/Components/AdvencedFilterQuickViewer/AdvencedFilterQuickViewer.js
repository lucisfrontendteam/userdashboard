import React, { PureComponent } from 'react';
import $ from 'jquery';
import UIContents from '../../Utils/UIContentsLoader';
const { TextRenderer, PropsCreator } = UIContents;

export class AdvencedFilterQuickViewer extends PureComponent {
  handleReset() {
    const { props } = this;
    const newProps = PropsCreator.getAddtionalProps(props);
    props.querystatemanagerclearInitAction(newProps);
  }

  handleShowAdvencedFilter() {
    $('#searchFilterLayer').modal('show');
  }

  handleInitAdvencedQuickViwer() {
    const { props } = this;
    const newProps = PropsCreator.getAddtionalProps(props);
    props.advencedfilterquickviewerInitAction(newProps);
  }

  handleShowAdvencedQuickViwer() {
    $('#advencedQuickViewer').show();
  }

  setAutoHide() {
    $('body').on('click', e => {
      const { id } = e.target;
      if (id !== 'showAdvencedFilter') {
        $('#advencedQuickViewer').hide();
      }
    });
  }

  componentDidMount() {
    this.setAutoHide();
  }

  unsetAutoHide() {
    $('body').off('click');
  }

  componentWillUnmount() {
    this.unsetAutoHide();
  }

  render() {
    const { props } = this;
    const {
      totalCount,
      filteredCount,
    } = props.AdvencedFilterQuickViewerState;
    const totalCnt = TextRenderer.renderNumber(totalCount);
    const filteredCnt = TextRenderer.renderNumber(filteredCount);
    return (
      <li className="dropdown filter-menu">
        <a
          href="#"
          className="dropdown-toggle"
          data-toggle="dropdown"
          role="button"
          aria-expanded="false"
          id="showAdvencedFilter"
          onClick={() => this.handleShowAdvencedQuickViwer()}
          onMouseOver={() => this.handleInitAdvencedQuickViwer()}
        >
          <span>필터</span>
        </a>
        <ul className="dropdown-menu" id="advencedQuickViewer">
          <li>
            <form>
              <p className="top-txt-filter">기본필터<br />({totalCnt})</p>
              <p className="top-txt-filter">필터<br />({filteredCnt})</p>
              <p className="top-func-area">
                <a onClick={() => this.handleReset()}>모두지우기</a>
                <a onClick={() => this.handleShowAdvencedFilter()}>필터설정</a>
              </p>
            </form>
          </li>
        </ul>
      </li>
    );
  }
}
