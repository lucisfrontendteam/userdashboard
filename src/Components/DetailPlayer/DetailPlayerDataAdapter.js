import _ from 'lodash';
import manifest from './manifest';
import scheme from './scheme';
import { validate } from 'jsonschema';
import UIContentsLoader from '../../Utils/UIContentsLoader';
const { TextRenderer } = UIContentsLoader;

export default class DetailPlayerDataAdapter {
  constructor(datas, options) {
    const result = validate(datas, scheme);
    this.errors = [];
    if (!!!result.valid) {
      const messages = _.map(result.errors, error => {
        const { property, message } = error;
        return `${property} ${message}`;
      });
      this.errors = [
        `${manifest.apiUrl} 을 점검하여 주십시오.`,
        ...messages,
      ];
    }
    this.detaultdatas = datas;
    this.totalCount = this.detaultdatas.grid.totalCount;
    this.detaultoptions = options;
  }

  get datas() {
    const datas = _.map(this.detaultdatas.grid.datas, record => {
      const {
        no,
        datetime,
        counselorName,
        telTimes,
        packRatio,
        category,
        keyword,
        customerRatio,
        counselorRatio,
        dupsRatio,
      } = record;
      return {
        no,
        datetime: TextRenderer.renderDateTimeString(datetime),
        counselorName,
        telTimes,
        packRatio,
        category,
        keyword,
        customerRatio,
        counselorRatio,
        dupsRatio,
      };
    });
    return datas;
  }

  get options() {
    return this.detaultoptions.grid.options;
  }

  get totalPageNumber() {
    // 그리드 데이터에서 취득해야 한다는 점에 주의
    const totalPageNumber = TextRenderer.renderTotalPages(this.totalCount, manifest.recordCount);
    return totalPageNumber;
  }

  get defaultData() {
    const { datas, options, totalPageNumber } = this;
    return {
      grid: {
        totalPageNumber,
        datas,
        options,
      },
    };
  }
}
