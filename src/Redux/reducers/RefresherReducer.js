import * as actionTypes from '../actionTypes';

export default function RefresherState(state = {}, action = null) {
  let finalState = {};
  switch (action.type) {
    case actionTypes.REFRESHER_INIT: {
      const { timeStamp } = action.payload;
      finalState = { timeStamp };
    } break;

    default: {
      finalState = { ...state };
    }
  }
  return finalState;
}
