import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { store } from './Redux';
import { MainPage } from './Containers/Pages';

window.onload = () => {
  const rootElement = document.getElementById('root');
  ReactDOM.render(
    <Provider store={store}>
      <MainPage />
    </Provider>,
    rootElement
  );
};
