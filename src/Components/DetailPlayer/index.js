import * as actions from './Redux/actions';
import * as reducer from './Redux/reducer';
export {
  actions as DetailPlayerActions,
  reducer as DetailPlayerReducer,
};
import manifest from './manifest';
export { manifest as DetailPlayerManifest };
import DetailPlayerSampleDatas from './DetailPlayerSampleDatas';
export { DetailPlayerSampleDatas };
export { DetailPlayer } from './DetailPlayer';
