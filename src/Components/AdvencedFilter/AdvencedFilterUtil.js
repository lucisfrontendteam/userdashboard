import _ from 'lodash';
import UIContents from '../../Utils/UIContentsLoader';
const { TreeTraveser } = UIContents;

export default class AdvencedFilterUtil {
  static filteredList(targetList) {
    const filteredList = _.filter(targetList, ({ id }) => !!!_.isEmpty(id));
    return filteredList;
  }

  static getNodeListFromOriginalTree(targetList, organizationTree) {
    const treeTraveser = new TreeTraveser(organizationTree);
    const filteredList = AdvencedFilterUtil.filteredList(targetList);
    const nodes = _.map(filteredList, ({ id: targetId }) => {
      const targetNode = treeTraveser.getNodeByPredict(node => {
        const originId = _.toString(node.model.id);
        const findTargetId = _.toString(targetId);
        return originId === findTargetId;
      });
      let targetModel = undefined;
      if (targetNode) {
        const { id, name } = targetNode.model;
        targetModel = { id, name };
      }
      return targetModel;
    });
    const nodeModels = _.compact(nodes);
    return nodeModels;
  }
}
