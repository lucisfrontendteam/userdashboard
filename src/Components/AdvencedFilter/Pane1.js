import React, { PureComponent } from 'react';
import $ from 'jquery';
import { v4 } from 'uuid';
import 'jquery-ui/ui/widgets/spinner';
import 'jquery-numeric';
import 'bootstrap-datepicker';
import 'bootstrap-datepicker/dist/locales/bootstrap-datepicker.kr.min.js';
import 'datepair.js';
import '../../Resources/js/jquery.timepicker';
import './Pane1.css';
import _ from 'lodash';

export default class Pane1 extends PureComponent {
  initializeTimePicker() {
    const { props } = this;
    this.$timeRanges
      = $(`#${props.dateTimeSelectId} .time`).timepicker({
        showDuration: true,
        timeFormat: 'a g:i',
        scrollDefault: 'now',
        lang: {
          am: '오전',
          pm: '오후',
          AM: '오전',
          PM: '오후',
          decimal: '.',
          mins: '분',
          hr: '시간',
          hrs: '시간',
        },
      });

    const {
      timeRangeToFilterState,
      timeRangeFromFilterState,
    } = props.QueryStateManagerState;

    this.$timeRanges.filter('.start')
      .timepicker('setTime', timeRangeFromFilterState);

    this.$timeRanges.filter('.end')
      .timepicker('setTime', timeRangeToFilterState);
  }

  initializeDatePair() {
    const { props } = this;
    const {
      dateRangeFromFilterState,
      dateRangeToFilterState,
    } = props.QueryStateManagerState;
    this.$dateRanges = $(`#${props.dateTimeSelectId} .date`).datepicker({
      format: 'yyyy년 m월 d일',
      autoclose: true,
      language: 'kr',
      todayHighlight: true,
      enableOnReadonly: true,
    });

    this.$dateRanges.filter('.start')
      .datepicker('setDate', dateRangeFromFilterState);

    this.$dateRanges.filter('.end')
      .datepicker('setDate', dateRangeToFilterState);

    // eslint-disable-next-line
    new window.Datepair(document.getElementById(props.dateTimeSelectId));
  }

  initializeSpinner() {
    const self = this;
    const { props } = self;
    const { recentDaysFilterState } = props.QueryStateManagerState;
    this.$dateRangeSpinner
      = $(document.getElementById(props.dateRangeSpinnerId)).spinner({
        min: 1,
        max: 120,
        change() {
          const $dateRange = self.$dateRangeSpinner;
          const isValid = $dateRange.spinner('isValid');
          let value = _.toNumber(this.value);
          if (!!!isValid) {
            const maxValue = $dateRange.spinner('option', 'max');
            const minValue = $dateRange.spinner('option', 'min');
            if (value > maxValue) {
              value = maxValue;
            } else {
              value = minValue;
            }
            $dateRange.spinner('value', value);
          }
        },
      }).val(recentDaysFilterState)
        .numeric();
  }

  destroySpinner() {
    // const { $dateRangeSpinner } = this;
    // if ($dateRangeSpinner) {
    //   $dateRangeSpinner.spinner('destroy');
    // }
  }

  destroyTimePicker() {
    // const { $timeRanges } = this;
    // if ($timeRanges) {
    //   $timeRanges.timepicker('destroy');
    // }
  }

  initializeTimeSlot() {
    const { props } = this;
    const { targetTimeSlotState } = props.QueryStateManagerState;
    if (!!!_.isEmpty(targetTimeSlotState)) {
      $(`#${props.timeSlotSelectId}`).val(targetTimeSlotState.id);
    }
  }

  initializeDateType() {
    const { props } = this;
    const { targetDateRangeState } = props.QueryStateManagerState;
    if (!!!_.isEmpty(targetDateRangeState)) {
      $(`input[type="radio"][value="${targetDateRangeState}"]`).prop('checked', true);
    }
  }

  componentDidMount() {
    this.initializeSpinner();
    this.initializeDatePair();
    this.initializeTimePicker();
    this.initializeTimeSlot();
    this.initializeDateType();
  }

  componentWillUnmount() {
    this.destroySpinner();
    this.destroyTimePicker();
  }

  componentDidUpdate() {
    this.destroySpinner();
    this.destroyTimePicker();
    this.initializeSpinner();
    this.initializeDatePair();
    this.initializeTimePicker();
    this.initializeTimeSlot();
    this.initializeDateType();
  }

  renderTimeSlotList() {
    const { props } = this;
    const { timeSlotList } = props.AdvencedFilterArgManagerState;
    return _.map(timeSlotList, timeSlot => {
      const { id, name } = timeSlot;
      const data = JSON.stringify(timeSlot);
      return (
        <option
          key={v4()}
          data-timeslot={data}
          value={id}
        >{name}</option>
      );
    });
  }

  render() {
    const { props } = this;
    const {
      dateRangeRadioName,
      allDataRangeId,
      recenctDataRangeId,
      rangeDataRangeId,
      dateTimeSelectId,
      dateRangeSpinnerId,
      timeSlotSelectId,
    } = props;
    const {
      targetDateRangeState: dateRangeType,
      targetTimeSlotState,
    } = props.QueryStateManagerState;
    const timeSlotState
      = _.isEmpty(targetTimeSlotState) ? { id: '' } : targetTimeSlotState;
    return (
      <div role="tabpanel" className="tab-pane" id="searchFilterSet02">
        <h2 className="title1">날짜 / 기간</h2>
        <div className="section-wrap">
          <h3 className="title2">날짜 및 기간설정</h3>
          <div className="innerbox">
            <table className="filter-table">
              <tbody>
                <tr>
                  <th>
                    <label className="radio-inline">
                      <input
                        type="radio"
                        id={allDataRangeId}
                        name={dateRangeRadioName}
                        value="ALL"
                        defaultChecked={(dateRangeType === 'ALL').toString()}
                      />
                      <label htmlFor={allDataRangeId}>전체기간</label>
                    </label>
                  </th>
                  <td></td>
                </tr>
                <tr>
                  <th>
                    <label className="radio-inline">
                      <input
                        type="radio"
                        id={recenctDataRangeId}
                        name={dateRangeRadioName}
                        value="RECENT"
                        defaultChecked={(dateRangeType === 'RECENT').toString()}
                      />
                      <label htmlFor={recenctDataRangeId}>최근일</label>
                    </label>
                  </th>
                  <td>
                    <input
                      id={dateRangeSpinnerId}
                      className="form-control"
                      defaultValue={1}
                    />
                  </td>
                </tr>
                <tr>
                  <th>
                    <label className="radio-inline">
                      <input
                        type="radio"
                        id={rangeDataRangeId}
                        name={dateRangeRadioName}
                        value="RANGE"
                        defaultChecked={(dateRangeType === 'RANGE').toString()}
                      />
                      <label htmlFor={rangeDataRangeId}>지정기간</label>
                    </label>
                  </th>
                  <td>
                    <div className="datepicker-wrap" id={dateTimeSelectId}>
                      <p>
                        <input
                          type="text"
                          className="form-control date start"
                          placeholder=""
                          readOnly="true"
                        />
                        <input
                          type="text"
                          className="form-control time start"
                          placeholder=""
                        />
                      </p>
                      <p className="rangeInformation">부터</p>
                      <p>
                        <input
                          type="text"
                          className="form-control date end"
                          placeholder=""
                          readOnly="true"
                        />
                        <input
                          type="text"
                          className="form-control time end"
                          placeholder=""
                        />
                      </p>
                      <p className="rangeInformation">까지</p>
                    </div>
                  </td>
                </tr>
                <tr>
                  <th><label className="radio-inline">시간대</label></th>
                  <td>
                    <label className="styled-select">
                      <select
                        id={timeSlotSelectId}
                        className="form-control"
                        defaultValue={timeSlotState.id}
                      >
                        {this.renderTimeSlotList()}
                      </select>
                    </label>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
