import { connect } from 'react-redux';
import { actions } from '../../Redux';
import { RouteScheduler } from '../../Components/RouteScheduler';

const mapStateToProps = (state) => state;

export const MainPage
  = connect(mapStateToProps, actions)(RouteScheduler);
