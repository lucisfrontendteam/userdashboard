import * as actionTypes from '../actionTypes';

export default function WidgetThemeListState(states = [], action = null) {
  let finalState = [];
  switch (action.type) {
    case actionTypes.WIDGET_THEME_LIST_INIT: {
      const { widgetThemeListState } = action.payload;
      finalState = finalState.concat(widgetThemeListState);
    } break;

    default: {
      finalState = finalState.concat(states);
    }
  }
  return finalState;
}
