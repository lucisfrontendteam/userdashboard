import * as actionTypes from './actionTypes';

export function DetailPlayerState(state = {}, action = null) {
  let finalState = {};
  switch (action.type) {
    case actionTypes.DETAIL_PLAYER_INIT: {
      const { widgetState } = action.payload;
      finalState = { ...widgetState };
    } break;

    case actionTypes.DETAIL_PLAYER_CLEAR: {
      finalState = { };
    } break;

    default: {
      finalState = { ...state };
    }
  }

  return finalState;
}
