import * as actionTypes from './actionTypes';
import { NetworkSwitcherSampleDatas } from '../NetworkSwitcherSampleDatas';

export function networkModeOffAction(
  networkModeState = NetworkSwitcherSampleDatas
) {
  return {
    type: actionTypes.NETWORK_MODE_OFF,
    payload: { networkModeState },
  };
}

export function networkModeOnAction(
  networkModeState = NetworkSwitcherSampleDatas
) {
  return {
    type: actionTypes.NETWORK_MODE_ON,
    payload: { networkModeState },
  };
}
