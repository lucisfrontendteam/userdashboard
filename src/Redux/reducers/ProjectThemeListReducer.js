import * as actionTypes from '../actionTypes';

export default function ProjectThemeListState(states = [], action = null) {
  let finalState = [];
  switch (action.type) {
    case actionTypes.PROJECT_THEME_LIST_INIT: {
      const { projectThemeListState } = action.payload;
      finalState = finalState.concat(projectThemeListState);
    } break;

    default: {
      finalState = finalState.concat(states);
    }
  }

  return finalState;
}
