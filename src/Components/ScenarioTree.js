import React, { PureComponent } from 'react';
import { Link } from 'react-router';
import { v4 } from 'uuid';
import _ from 'lodash';
import UIContents from '../Utils/UIContentsLoader';
const { ProjectWriter, EnvironmentManager } = UIContents;
import environment from '../../config/environment';

export class ScenarioTree extends PureComponent {
  static defaultProps = {
    showStyle: {
      display: 'block',
    },
    hideStyle: {
      display: 'none',
    },
  };

  constructor(props) {
    super(props);
    const manager = new EnvironmentManager(environment);
    this.rootPath = manager.rootPath;
  }

  renderScenarios(parentModel) {
    const { props, rootPath } = this;
    const selectedBranch = ProjectWriter.getSelectedScenarioBranch(props);
    return _.map(parentModel.children, nodeModel => {
      const { uri, name } = nodeModel.model;
      const projectNode = selectedBranch[0];
      const selectedScenarioRoot = selectedBranch[1];
      const selectedScenario = selectedBranch[2];
      const scenarioPath =
        `${rootPath}/${projectNode.model.accessPath}/${selectedScenarioRoot.model.uri}/${uri}`;
      const scenarioSelected = selectedScenario.model.uri === uri;
      const activeClassName = scenarioSelected ? 'active' : '';
      const scenarioRootSelected = selectedScenarioRoot.model.uri === parentModel.model.uri;
      const visibility = scenarioRootSelected ? props.showStyle : props.hideStyle;

      return (
        <li key={v4()} style={visibility}>
          <Link to={scenarioPath} activeClassName={activeClassName}>{name}</Link>
        </li>
      );
    });
  }

  renderScenarioRoots() {
    const { props, rootPath } = this;
    const selectedBranch = ProjectWriter.getSelectedScenarioBranch(props);
    return _.map(props.depth1Models, nodeModel => {
      const { uri, name } = nodeModel.model;
      const selectedScenarioRoot = selectedBranch[1];
      const selected = selectedScenarioRoot.model.uri === uri;
      const visibility = selected ? props.showStyle : props.hideStyle;
      const activeClassName = selected ? 'active' : '';
      const firstScenario = nodeModel.children[0];
      const projectNode = selectedBranch[0];
      const firstScenarioPath =
        `${rootPath}/${projectNode.model.accessPath}/${uri}/${firstScenario.model.uri}`;

      return (
        <li className={activeClassName} key={v4()}>
          <Link to={firstScenarioPath} >{name}</Link>
          <ul className="dep2" style={visibility}>
            {this.renderScenarios(nodeModel)}
          </ul>
        </li>
      );
    });
  }

  render() {
    return (
      <ul className="dep1">
        {this.renderScenarioRoots()}
      </ul>
    );
  }
}
